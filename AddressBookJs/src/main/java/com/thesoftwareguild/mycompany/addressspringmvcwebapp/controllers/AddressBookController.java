/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.controllers;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.validation.ValidationError;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.validation.ValidationErrorContainer;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.AddressDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.ContactDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.LocationDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.StreetDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Address;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.AddressCommandObj;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Contact;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.ContactCommandObj;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Location;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Street;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/address")
public class AddressBookController {

    private AddressDao addressDao;
    private ContactDao contactDao;
    private StreetDao streetDao;
    private LocationDao locationDao;

    @Inject
    public AddressBookController(AddressDao addressDao, ContactDao contactDao,
            StreetDao streetDao, LocationDao locationDao) {
        this.addressDao = addressDao;
        this.contactDao = contactDao;
        this.streetDao = streetDao;
        this.locationDao = locationDao;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> create(@Valid @RequestBody ContactCommandObj request) {
        if (contactDao.doesExist(request.getFirstName(), request.getLastName())) {

            List<Contact> contacts = contactDao.get(request.getFirstName(), request.getLastName());
            ValidationErrorContainer container = new ValidationErrorContainer();

            contacts.stream().forEach((current) -> {
                container.addError(new ValidationError(current.getContactId().toString(), "id"));
            });

            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(container);
        }
        Street street = new Street();
        street.setStreetName(request.getStreetName());
        street.setStreetNumber(request.getStreetNumber());
        Street addressStreet = streetDao.add(street);

        Location location = new Location();
        location.setCity(request.getCity());
        location.setState(request.getState());
        location.setZip(request.getZip());
        Location addressLocation = locationDao.add(location);

        List<Address> firstAddition = new ArrayList();
        Address address = new Address();
        address.setLocation(addressLocation);
        address.setStreet(addressStreet);
        Address contactAddress = addressDao.add(address);
        firstAddition.add(contactAddress);

        Contact contact = new Contact();
        contact.setFirstName(request.getFirstName());
        contact.setLastName(request.getLastName());
        contact.setKnownAddresses(firstAddition);
        Contact returnContact = contactDao.add(contact);

        return ResponseEntity.status(HttpStatus.OK).body(returnContact);
    }

    @RequestMapping(value = "/duplicate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> createDuplicate(@Valid @RequestBody ContactCommandObj request) {

        Street street = new Street();
        street.setStreetName(request.getStreetName());
        street.setStreetNumber(request.getStreetNumber());
        Street addressStreet = streetDao.add(street);

        Location location = new Location();
        location.setCity(request.getCity());
        location.setState(request.getState());
        location.setZip(request.getZip());
        Location addressLocation = locationDao.add(location);

        List<Address> firstAddition = new ArrayList();
        Address address = new Address();
        address.setLocation(addressLocation);
        address.setStreet(addressStreet);
        Address contactAddress = addressDao.add(address);
        firstAddition.add(contactAddress);

        List<Contact> validationCheckList = contactDao.get(request.getFirstName(), request.getLastName());

        for (Contact current : validationCheckList) {
            if (contactDao.isADuplicate(current, contactAddress)) {
                return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
            }
        }

        Contact contact = new Contact();
        contact.setFirstName(request.getFirstName());
        contact.setLastName(request.getLastName());
        contact.setKnownAddresses(firstAddition);
        Contact returnContact = contactDao.add(contact);

        return ResponseEntity.status(HttpStatus.OK).body(returnContact);
    }

    @RequestMapping(value = "/{id}")
    @ResponseBody
    public Contact view(@PathVariable("id") Integer contactId) {

        Contact contact = contactDao.get(contactId);

        contact.setKnownAddresses(addressDao.getAddressListForContact(contact.getContactId()));

        return contact;

    }

    @RequestMapping(value = "/address/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Address get(@PathVariable("id") Integer addressId) {

        return addressDao.get(addressId);

    }

    @RequestMapping(value = "/{id}/{adid}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> edit(@Valid @RequestBody AddressCommandObj request,
            @PathVariable("id") Integer id,
            @PathVariable("adid") Integer addressId) {
        Street street = new Street();
        street.setStreetName(request.getStreetName());
        street.setStreetNumber(request.getStreetNumber());
        Street addressStreet = streetDao.add(street);

        Location location = new Location();
        location.setCity(request.getCity());
        location.setState(request.getState());
        location.setZip(request.getZip());
        Location addressLocation = locationDao.add(location);

        Address address = new Address();
        address.setLocation(addressLocation);
        address.setStreet(addressStreet);
        Address contactAddress = null;
        if (addressDao.doesExist(address)) {
            contactAddress = addressDao.getByVals(address);
        } else {
            contactAddress = addressDao.add(address);
        }

        Contact contact = contactDao.get(id);
        if (contactDao.isADuplicate(contact, contactAddress)) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        } else {
            contact.setKnownAddresses(addressDao.getAddressListForContact(contact.getContactId()));
            contact.removeKnownAddress(addressId);
            contact.addAddressToKnown(contactAddress);
            contactDao.update(contact);
        };

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer contactId) {
        contactDao.delete(contactId);

    }
}
