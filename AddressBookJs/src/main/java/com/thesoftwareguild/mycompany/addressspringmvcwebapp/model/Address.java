package com.thesoftwareguild.mycompany.addressspringmvcwebapp.model;


public class Address {

    private Integer addressId;
    //foreign keys
    private Location location;
    private Street street;
    
    public Address(){
        
    }
    
    public Address(Integer addressId, Location locationId, Street streetId){
        this.addressId= addressId;
        this.location = locationId;
        this.street = streetId;
    }

    /**
     * @return the addressId
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     * @param addressId the addressId to set
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

       /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the street
     */
    public Street getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(Street street) {
        this.street = street;
    }
}