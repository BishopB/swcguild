/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.model;

import java.util.List;

/**
 *
 * @author apprentice
 */
public class Contact {
    //pk
    private Integer contactId;
    //attributes
    private List<Address> knownAddresses;
    private String firstName;
    private String lastName;
    
    public Contact(){
        
    }
    public Contact(Integer contactId, List<Address> knownAddresses, 
            String firstName, String lastName){
        //method body
        this.contactId=contactId;
        this.knownAddresses= knownAddresses;
        this.firstName= firstName;
        this.lastName= lastName;
    }

    /**
     * @return the contactId
     */
    public Integer getContactId() {
        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    /**
     * @return the knownAddresses
     */
    public List<Address> getKnownAddresses() {
        return knownAddresses;
    }

    /**
     * @param knownAddresses the knownAddresses to set
     */
    public void setKnownAddresses(List<Address> knownAddresses) {
        this.knownAddresses = knownAddresses;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public void addAddressToKnown(Address address){
        this.knownAddresses.add(address);
    }
    
    public void removeKnownAddress(Integer id){
        knownAddresses.remove(knownAddresses.stream()
                .filter(add -> add
                        .getAddressId()
                        .equals(id))
                .findFirst()
                .get());
        
        
    }
}
