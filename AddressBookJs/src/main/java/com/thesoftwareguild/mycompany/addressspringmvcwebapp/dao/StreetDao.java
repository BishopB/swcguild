/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Street;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class StreetDao {

    private JdbcTemplate template;
    private static final String SQL_INSERT_STREET = "INSERT INTO `street`(`street_number`, `street_name`)VALUES(?, ?);";
    private static final String SQL_GET_STREET = "SELECT * FROM `street` WHERE id = ?;";
    private static final String SQL_UPDATE_STREET = "UPDATE `street` SET `street_number` = ?, `street_name` = ? WHERE `id` = ?;";
    private static final String SQL_DELETE_STREET = "DELETE FROM `street` WHERE `id` = ?;";
    private static final String SQL_VALIDATE_STREET = "SELECT count(*) FROM `street` WHERE street_number = ? AND street_name = ?;";
    private static final String SQL_GET_STREET_BY_NUM_AND_NAME = "SELECT * FROM `street` WHERE street_number = ? AND street_name = ?;";
    
    public StreetDao(JdbcTemplate templates) {
        this.template = templates;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Street add(Street street) {
        //VALIDATION
        if (doesExist(street)) {
            //get by name
            return getByNumberAndName(street);
        } else {
            template.update(SQL_INSERT_STREET,
                    street.getStreetNumber(),
                    street.getStreetName());

            street.setStreetId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));

            return street;
        }
    }

    public Street get(Integer id) {
        return template.queryForObject(SQL_GET_STREET, new StreetMapper(), id);
    }
    
    public Street getByNumberAndName(Street street){
        return template.queryForObject(SQL_GET_STREET_BY_NUM_AND_NAME, 
                new StreetMapper(),
                street.getStreetNumber(),
                street.getStreetName());
    }

    public void update(Street street) {
        template.update(SQL_UPDATE_STREET,
                street.getStreetNumber(),
                street.getStreetName(),
                street.getStreetId());
    }

    public void delete(Integer id) {
        template.update(SQL_DELETE_STREET, id);
    }

    public List<Street> list() {
        return new ArrayList<>();
    }

    private static class StreetMapper implements RowMapper<Street> {

        public StreetMapper() {
        }

        @Override
        public Street mapRow(ResultSet rs, int i) throws SQLException {

                Street street = new Street();

                street.setStreetId(rs.getInt("id"));
                street.setStreetNumber(rs.getString("street_number"));
                street.setStreetName(rs.getString("street_name"));

                return street;
            }

    }

    private boolean doesExist(Street street) {
        Long cnt = template.queryForObject(
                SQL_VALIDATE_STREET, Long.class,
                street.getStreetNumber(),
                street.getStreetName());

        return cnt != null && cnt > 0;

    }

}
