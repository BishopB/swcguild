/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Location;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class LocationDao {

    private JdbcTemplate template;
    private static final String SQL_INSERT_LOCATION = "INSERT INTO `location`(`city`, `state`, `zip`)VALUES(?,?,?);";
    private static final String SQL_GET_LOCATION = "SELECT * FROM `location` WHERE id = ?;";
    private static final String SQL_UPDATE_LOCATION = "UPDATE `location` SET `city` = ?, `state` = ?, `zip` = ? WHERE `id` = ?;";
    private static final String SQL_DELETE_LOCATION = "DELETE FROM `location` WHERE `id` = ?;";
    private static final String SQL_VALIDATE_LOCATION = "SELECT count(*) FROM `location` WHERE city = ? AND state = ? AND zip = ?;";
    private static final String SQL_GET_LOCATION_BY_VALUES = "SELECT * FROM `location` WHERE city = ? AND state = ? AND zip = ?;";

    public LocationDao(JdbcTemplate template) {
        this.template = template;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Location add(Location location) {
        if (doesExist(location)) {
            return getByValues(location);
        } else {
            template.update(SQL_INSERT_LOCATION,
                    location.getCity(),
                    location.getState(),
                    location.getZip());

            location.setLocationId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));

            return location;
        }
    }

    public Location get(Integer id) {
        return template.queryForObject(SQL_GET_LOCATION, new LocationMapper(), id);
    }
    
    public Location getByValues(Location location){
        return template.queryForObject(SQL_GET_LOCATION_BY_VALUES, 
                new LocationMapper(),
                location.getCity(),
                location.getState(),
                location.getZip());
    }

    public void update(Location location) {
        template.update(SQL_UPDATE_LOCATION,
                location.getCity(),
                location.getState(),
                location.getZip(),
                location.getLocationId());
    }

    public void delete(Integer id) {
        template.update(SQL_DELETE_LOCATION, id);
    }

    public List<Location> list() {
        return new ArrayList<>();
    }

    private static class LocationMapper implements RowMapper<Location> {

        public LocationMapper() {
        }

        @Override
        public Location mapRow(ResultSet rs, int i) throws SQLException {
            Location location = new Location();

            location.setLocationId(rs.getInt("id"));
            location.setCity(rs.getString("City"));
            location.setState(rs.getString("State"));
            location.setZip(rs.getString("Zip"));

            return location;
        }
    }

    private boolean doesExist(Location location) {
        Long cnt = template.queryForObject(
                SQL_VALIDATE_LOCATION, Long.class,
                location.getCity(),
                location.getState(),
                location.getZip());

        return cnt != null && cnt > 0;

    }
}
