/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao;
//SELECT * FROM address ad INNER JOIN contact_address ca ON ca.contact_id=1 AND ca.address_id = ad.id;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Address;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Contact;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class ContactDao {

    private AddressDao addressDao;
    private JdbcTemplate template;
    private static final String SQL_INSERT_CONTACT = "INSERT INTO `contact`(`first_name`,`last_name`)VALUES(?,?);";
    private static final String SQL_GET_CONTACT = "SELECT * FROM contact WHERE id = ?";
    private static final String SQL_DELETE_CONTACT = "DELETE FROM `contact` WHERE `id` = ?;";
    private static final String SQL_INSERT_JOIN_TABLE = "INSERT INTO contact_address (contact_id, address_id) VALUES (?, ?);";
    private static final String SQL_DELETE_FROM_JOIN_TABLE = "DELETE FROM contact_address WHERE contact_id = ?";
    private static final String SQL_LIST_CONTACTS = "SELECT * FROM contact";
    private static final String SQL_VALIDATE_CONTACT = "SELECT count(*) FROM `contact` WHERE first_name = ? AND last_name = ?;";
    private static final String SQL_GET_CONTACT_BY_VALUES = "SELECT * FROM `contact` WHERE first_name = ? AND last_name = ?;";
    private static final String SQL_VALIDATE_DUPLICATE_CONTACT = "SELECT count(*) FROM contact_address WHERE contact_id = ? AND address_id = ?;";
    private static final String SQL_DELETE_ONE_FROM_JOIN_TABLE = "DELETE FROM contact_address WHERE contact_id = ? AND address_id = ?";

    public ContactDao(JdbcTemplate template, AddressDao addressDao) {
        this.template = template;
        this.addressDao = addressDao;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Contact add(Contact contact) {
        //CONTROLLER LIST<ADDRESS> ADDITION NEEDS TO HAVE HAPPENED
        template.update(SQL_INSERT_CONTACT,
                contact.getFirstName(),
                contact.getLastName());

        contact.setContactId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));
        updateJoinTable(contact);
        return contact;
    }

    public Contact get(Integer id) {
        Contact contact = template.queryForObject(SQL_GET_CONTACT, new ContactMapper(), id);
        contact.setKnownAddresses(addressDao.getAddressListForContact(id));
        return contact;
    }
    
    public List<Contact> get(String firstName, String lastName){
        return template.query(SQL_GET_CONTACT_BY_VALUES, 
                new ContactMapper(),
                firstName,
                lastName);
    }
    
    public void update(Contact contact) {
            //UPDATE MANY2MANY T4BLE
        template.update(SQL_DELETE_FROM_JOIN_TABLE, contact.getContactId());
        updateJoinTable(contact);
    }

    public void delete(Integer id) {
        template.update(SQL_DELETE_FROM_JOIN_TABLE, id);
        template.update(SQL_DELETE_CONTACT, id);
    }

    public List<Contact> list() {
        return template.query(SQL_LIST_CONTACTS, new ContactMapper());
    }

    private void updateJoinTable(Contact contact) throws DataAccessException {
        for (Address current : contact.getKnownAddresses()) {
            template.update(SQL_INSERT_JOIN_TABLE,
                    contact.getContactId(),
                    current.getAddressId());
        }
    }

    private class ContactMapper implements RowMapper<Contact> {

        public ContactMapper() {
        }

        @Override
        public Contact mapRow(ResultSet rs, int i) throws SQLException {
            Contact contact = new Contact();

            contact.setFirstName(rs.getString("first_name"));
            contact.setContactId(rs.getInt("id"));
            contact.setLastName(rs.getString("last_name"));

            return contact;
        }
    }

    public boolean doesExist(String firstName, String lastName) {
        Long cnt = template.queryForObject(
                SQL_VALIDATE_CONTACT, Long.class,
                firstName,
                lastName);

        return cnt != null && cnt > 0;

    }
    
    public boolean isADuplicate(Contact contact, Address address) {
        Long cnt = template.queryForObject(
                SQL_VALIDATE_DUPLICATE_CONTACT, Long.class,
                contact.getContactId(),
                address.getAddressId());

        return cnt != null && cnt > 0;

    }

}
//SELECT * FROM address ad 
//INNER JOIN contact_address ca 
//INNER JOIN contact co 
//INNER JOIN location lo 
//INNER JOIN street st 
//ON ca.contact_id=3 
//AND ca.address_id = ad.id 
//AND ad.location_id = lo.id 
//AND ad.street_id = st.id 
//AND co.id = ca.contact_id;
