/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.validation;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class ValidationErrorContainer {
    
    private List<ValidationError> errors = new ArrayList();
    
    public void addError(ValidationError error){
        getErrors().add(error);
    }

    public List<ValidationError> getErrors() {
        return errors;
    }
    
}
