package com.thesoftwareguild.mycompany.addressspringmvcwebapp.controllers;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.AddressDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.ContactDao;
import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Contact;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {
     private AddressDao addressDao;
     private ContactDao contactDao;
     
    @Inject
    public HelloController(ContactDao contactDao, AddressDao adDao) {
        this.addressDao=adDao;
        this.contactDao = contactDao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Model model) {
        
        List<Contact> contactList = contactDao.list();
        for(Contact current: contactList){
            current.setKnownAddresses(addressDao.getAddressListForContact(current.getContactId()));
        }
        model.addAttribute("addressList", contactList);
        
       return "index";
    }
}
