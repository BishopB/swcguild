/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.model;

/**
 *
 * @author apprentice
 */
public class Street {
    //pk
    private Integer streetId;
    //attributes
    private String streetNumber;
    private String streetName;
    
    public Street(){
        
    }
    
    public Street(Integer streetId, String streetNumber, String streetName){
        this.streetId= streetId;
        this.streetName = streetName;
        this.streetNumber= streetNumber;
    }

    /**
     * @return the streetId
     */
    public Integer getStreetId() {
        return streetId;
    }

    /**
     * @param streetId the streetId to set
     */
    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    /**
     * @return the streetNumber
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * @param streetNumber the streetNumber to set
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * @return the streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * @param streetName the streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
}
