/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao;

import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AddressDao {

    private LocationDao locationDao;
    private StreetDao streetDao;
    private JdbcTemplate template;
    private static final String SQL_INSERT_ADDRESS = "INSERT INTO `address`(`location_id`, `street_id`)VALUES(?,?);";
    private static final String SQL_GET_ADDRESS = "SELECT * FROM address WHERE id = ?";
    private static final String SQL_GET_ADDRESS_LIST = "SELECT ca.contact_id, ad.id, ad.location_id, ad.street_id FROM address ad INNER JOIN contact_address ca ON ca.address_id = ad.id WHERE ca.contact_id=?; ";
    private static final String SQL_UPDATE_ADDRESS = "UPDATE address SET location_id = ?, street_id = ? WHERE id = ?";
    private static final String SQL_DELETE_ADDRESS = "DELETE FROM `address` WHERE `id` = ?;";
    private static final String SQL_VALIDATE_ADDRESS = "SELECT count(*) FROM `address` WHERE location_id = ? AND street_id = ?;";
    private static final String SQL_GET_ADDRESS_BY_VALUES = "SELECT * FROM `address` WHERE location_id = ? AND street_id = ?;";

    public AddressDao(JdbcTemplate template, LocationDao locationDao, StreetDao streetDao) {
        this.template = template;
        this.locationDao = locationDao;
        this.streetDao = streetDao;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Address add(Address address) {
        if (doesExist(address)) {
            return getByVals(address);
        } else {
            template.update(SQL_INSERT_ADDRESS, address.getLocation().getLocationId(), address.getStreet().getStreetId());

            address.setAddressId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));

            return address;
        }
    }

    public Address get(Integer id) {
        return template.queryForObject(SQL_GET_ADDRESS, new AddressMapper(), id);
    }
    
    public Address getByVals(Address address){
        return template.queryForObject(SQL_GET_ADDRESS_BY_VALUES, 
                new AddressMapper(),
                address.getLocation().getLocationId(),
                address.getStreet().getStreetId());
    }

    public List<Address> getAddressListForContact(Integer id) {
        return template.query(SQL_GET_ADDRESS_LIST, new AddressMapper(), id);
    }

    public void update(Address address) {
        //CONTROLLER VALIDATE LOCATION AND STREET
        template.update(SQL_UPDATE_ADDRESS,
                address.getLocation().getLocationId(),
                address.getStreet().getStreetId(),
                address.getAddressId());
    }

    public void delete(Integer id) {
        template.update(SQL_DELETE_ADDRESS, id);
    }

    public List<Address> list() {
        return new ArrayList<>();
    }

    private class AddressMapper implements RowMapper<Address> {

        public AddressMapper() {
        }

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            Address address = new Address();

            address.setAddressId(rs.getInt("id"));
            address.setLocation(locationDao.get(rs.getInt("location_id")));
            address.setStreet(streetDao.get(rs.getInt("street_id")));
            return address;
        }
    }

    public boolean doesExist(Address address) {
        Long cnt = template.queryForObject(
                SQL_VALIDATE_ADDRESS, Long.class,
                address.getLocation().getLocationId(),
                address.getStreet().getStreetId());

        return cnt != null && cnt > 0;

    }
}
