<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Search Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class ="container">
            <jsp:include page="navbar.jsp" />
            <div class="row">

                <div class ="col-md-10">
                    <h2 >Address Search</h2>
                    <form:form class ="form-horizontal" action="${pageContext.request.contextPath}/search" method="post">
                        <div class="form-group">
                            <label for="add-search-value" class="col-md-4 control-label">Street Number:</label>
                            <div class="col-md-8">
                                <input type="text" name="searchValue" class="form-control" id="add-search-value" placeholder="Search by last name, city, state, or zip" />
                            </div>
                        </div>

                        <input type="submit" class="btn btn-default pull-right" name="searchMode" value="Last Name"/>
                        <input type="submit" class="btn btn-default pull-right" name="searchMode" value="City"/>
                        <input type="submit" class="btn btn-default pull-right" name="searchMode" value="State"/>
                        <input type="submit" class="btn btn-default pull-right" name="searchMode" value="Zip Code"/>
                    </form:form>
                </div>
            </div>
            <div class ="row">
                <div class ="col-md-6 col-md-push-3 text-center">

                    <c:if test="${results.size()>=0}">
                        <hr />
                        <c:choose>
                            <c:when test="${results.size()>0}">
                                <c:forEach items="${results}" var = "result">
                                    <h3>${result.firstName} ${result.lastName}</h3>
                                    <div>
                                        <blockquote>
                                            <p>Address: ${result.streetNumber} ${result.streetName}</p>
                                            <p>City, State: ${result.city}, ${result.state}</p>
                                            <p>Zip Code: ${result.zip}</p>
                                        </blockquote>
                                    </div>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <blockquote>
                                    <br />
                                    <p>No Results! There are no Address's with "${searchValue}" as the ${searchMode}</p>
                                    <br />
                                </blockquote>
                            </c:otherwise>
                        </c:choose>        
                    </c:if>

                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
