<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class ="container">
            <jsp:include page="navbar.jsp" />
            <div class="row">
                <div class ="col-md-6">

                    <div class ="col-md-6">
                        <h2>Edit Address</h2>
                        <form:form class ="form-horizontal" commandName = "address" action="${pageContext.request.contextPath}/address/edit/${address.id}" method="post">
                            <div class="form-group">
                                <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                                <div class="col-md-8">
                                    <form:input path="firstName" type="text" name="firstName" class="form-control" id="add-first-name" placeholder="First Name" />
                                    <form:errors path="firstName" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                                <div class="col-md-8">
                                    <form:input path="lastName" type="text" name="lastName" class="form-control" id="add-last-name" placeholder="Last Name" />
                                    <form:errors path="lastName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-street-number" class="col-md-4 control-label">Street Number:</label>
                                <div class="col-md-8">
                                    <form:input path="streetNumber" type="text" name="streetNumber" class="form-control" id="add-street-number" placeholder="Street Number" />
                                    <form:errors path="streetNumber"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-street-name" class="col-md-4 control-label">Street Name:</label>
                                <div class="col-md-8">
                                    <form:input path="streetName" type="text" name="streetName" class="form-control" id="add-street-name" placeholder="Street Name" />
                                    <form:errors path="streetName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-8">
                                    <form:input path="city" type="text" name="city" class="form-control" id="add-city" placeholder="City" />
                                    <form:errors path="city"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-8">
                                    <form:input path="state" type="text" name="state" class="form-control" id="add-state" placeholder="State" />
                                    <form:errors path="state"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-zip" class="col-md-4 control-label">Zip Code:</label>
                                <div class="col-md-8">
                                    <form:input path="zip" type="text" name="zip" class="form-control" id="add-zip" placeholder="Zip" />
                                    <form:errors path="zip"/>
                                </div>
                            </div>

                            <input type="submit" class="btn btn-default pull-right" value="Create"/>
                        </form:form>
                    </div>
                </div>
            </div>
            <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
