

$(document).ready(function () {

    $(document).on('submit', '#create-address-form', function (e) {
        $(".errorMsg").empty();
        e.preventDefault();
        var addressObject = {
            firstName: $('#add-first-name').val(),
            lastName: $('#add-last-name').val(),
            streetNumber: $('#add-street-number').val(),
            streetName: $('#add-street-name').val(),
            city: $('#add-city').val(),
            state: $('#add-state').val(),
            zip: $('#add-zip').val()
        };
        var addressData = JSON.stringify({
            firstName: $('#add-first-name').val(),
            lastName: $('#add-last-name').val(),
            streetNumber: $('#add-street-number').val(),
            streetName: $('#add-street-name').val(),
            city: $('#add-city').val(),
            state: $('#add-state').val(),
            zip: $('#add-zip').val()
        });
        $.ajax({
            type: 'POST',
            url: contextRoot + "/address",
            data: addressData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            statusCode: {
                418: function (data) {
                    console.log(data);
                    var errors = data.responseJSON.errors;
                    $.each(errors, function (index, validationError) {
                        console.log(validationError.message);
                        var teapotRow = buildTeapotTableRow(validationError.message, addressObject.firstName, addressObject.lastName);
                        $('#teapot-modal-table').append(teapotRow);
                    });
                    $.each(addressObject, function (key, value) {
                        console.log(key);
                        console.log(value);
                        $('#hidden-' + key).val(value);
                    });
                    $('#errorTeaPot').modal('toggle');
                },
                400: function (data) {
                    var errors = data.responseJSON.errors;
                    $.each(errors, function (index, validationError) {
                        $('#create-entry-error-' + validationError.fieldName)
                                .append("* " + validationError.message)
                                .append("<br />");
                    });
                }
            }
        }).success(function (data, status) {
//Clear Form
            $('#add-first-name').val("");
            $('#add-last-name').val("");
            $('#add-street-number').val("");
            $('#add-street-name').val("");
            $('#add-city').val("");
            $('#add-state').val("");
            $('#add-zip').val("");
            var tableRow = buildAddressRow(data);
            $('#addressTable').append($(tableRow));
        });
    });

    $(document).on('click', '#duplicate', function (e) {
        e.preventDefault();
        var addressObject = {
            firstName: $('#add-first-name').val(),
            lastName: $('#add-last-name').val(),
            streetNumber: $('#add-street-number').val(),
            streetName: $('#add-street-name').val(),
            city: $('#add-city').val(),
            state: $('#add-state').val(),
            zip: $('#add-zip').val()
        };
        var addressData = JSON.stringify({
            firstName: $('#add-first-name').val(),
            lastName: $('#add-last-name').val(),
            streetNumber: $('#add-street-number').val(),
            streetName: $('#add-street-name').val(),
            city: $('#add-city').val(),
            state: $('#add-state').val(),
            zip: $('#add-zip').val()
        });
        $.ajax({
            type: 'POST',
            url: contextRoot + "/address/duplicate",
            data: addressData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            statusCode: {
                418: function (data) {
                    alert("Already Exists!!!");
                    $('.modal').modal('hide');
                    $('#add-first-name').val("");
                    $('#add-last-name').val("");
                    $('#add-street-number').val("");
                    $('#add-street-name').val("");
                    $('#add-city').val("");
                    $('#add-state').val("");
                    $('#add-zip').val("");
                },
                400: function (data) {
                    var errors = data.responseJSON.errors;
                    $.each(errors, function (index, validationError) {
                        $('#create-entry-error-' + validationError.fieldName)
                                .append("* " + validationError.message)
                                .append("<br />");
                    });
                }
            }
        }).success(function (data, status) {
//Clear Form
            $('#add-first-name').val("");
            $('#add-last-name').val("");
            $('#add-street-number').val("");
            $('#add-street-name').val("");
            $('#add-city').val("");
            $('#add-state').val("");
            $('#add-zip').val("");
            var tableRow = buildAddressRow(data);
            $('#addressTable').append($(tableRow));
        });
    });

    $('#showAddressModal').on('show.bs.modal', function (e) {

        $('#show-modal-table tr.address-view-row').remove();
        $('#errorTeaPot').modal('hide');
        var link = $(e.relatedTarget);
        var addressId = link.data('address-id');
        var modal = $(this);
        $.ajax({
            type: 'GET',
            url: contextRoot + '/address/' + addressId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (address, status) {
            $.each(address.knownAddresses, function (key, value) {
                var viewAddressRow = buildAddressViewRow(value, key);
                $('#show-modal-table').append($(viewAddressRow));
            });
            modal.find('#address-first-name').text(address.firstName);
            modal.find('#address-last-name').text(address.lastName);
        });
    });

    $('#showEditModal').on('show.bs.modal', function (e) {

        $('#edit-choice-modal-table tr.address-view-edit-row').remove();
        $('#errorTeaPot').modal('hide');
        var link = $(e.relatedTarget);
        var contactId = link.data('address-id');
        var modal = $(this);
        $.ajax({
            type: 'GET',
            url: contextRoot + '/address/' + contactId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (address, status) {
            $('#show-edit-view-header').text(address.firstName + " " + address.lastName);
            $.each(address.knownAddresses, function (key, value) {
                var showEditAddressRow = buildAddressShowEditRow(value, key);
                $('#edit-choice-modal-table').append($(showEditAddressRow));
            });
            $('#addressAddition').val(contactId);
        });
    });

    $(document).on('click', '#edit-address-button', function (e) {
        e.preventDefault();
        var contactId = $('#addressAddition').val();
        var addressId = $('#edit-address-button').val();
        var data = JSON.stringify({
            streetNumber: $('#edit-street-number').val(),
            streetName: $('#edit-street-name').val(),
            city: $('#edit-city').val(),
            state: $('#edit-state').val(),
            zip: $('#edit-zip').val()
        });
        $.ajax({
            type: 'PUT',
            url: contextRoot + '/address/' + contactId + "/" + addressId,
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            statusCode: {
                418: function (data) {
                    alert("Already Exists!!!");
                    $('.modal').modal('hide');
                }}

        }).success(function (data, status) {
            $('#editActualAddressModal').modal('hide');
        });
    });

    $(document).on('click', '.delete-link', function (e) {

        e.preventDefault();
        var contactId = $(e.target).data('address-id');
        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/address/' + contactId
        }).success(function (data, status) {
            $('#address-row-' + contactId).remove();
        });
    });

    $(document).on('click', '.editAddressLink', function (e) {
        e.preventDefault();
        var link = $(e.target);
        console.log(e.target.nodeName);
        var addressId = link.data('address-id');
        var modal = $(this);
        $.ajax({
            type: 'GET',
            url: contextRoot + '/address/address/' + addressId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (address, status) {
            $('#edit-street-name').val(address.street.streetName);
            $('#edit-street-number').val(address.street.streetNumber);
            $('#edit-city').val(address.location.city);
            $('#edit-state').val(address.location.state);
            $('#edit-zip').val(address.location.zip);
            $('#showEditModal').modal('hide');
            $('#editActualAddressModal').modal('toggle');
            $('#edit-address-button').val(addressId);
        });
    });
    
    function buildAddressRow(data) {
        return "<tr id='address-row-" + data.contactId + "'> \n\
                   <td><a data-address-id ='" + data.contactId + "' data-toggle='modal' data-target='#showAddressModal'>" + data.lastName + ", " + data.firstName + " </a></td> \n\
                   <td><a data-address-id ='" + data.contactId + "' data-toggle='modal' data-target='#showEditModal'>Edit</a></td>\n\
                   <td><a data-address-id='" + data.contactId + "' class='delete-link'>Delete</a></td>\n\
                   </tr> ";
    }

    function buildAddressViewRow(data, number) {
        number = number + 1;
        return "<tr class = 'address-view-row'> \n\
         <th>Address " + number + "</th> \n\
         <td>" + data.street.streetNumber + " " + data.street.streetName + "</td>\n\
         </tr>\n\
         <tr class = 'address-view-row'> \n\
         <th></th>\n\
         <td>" + data.location.city + ", " + data.location.state + ", " + data.location.zip + "</td>\n\
         </tr>";
    }

    function buildTeapotTableRow(number, firstName, lastName) {
        return "<tr class = 'teapot-row-'" + number + "> \n\
         <th>" + firstName + " " + lastName + "</th> \n\
        <td><a data-address-id ='" + number + "' data-toggle='modal' data-target='#showAddressModal'>View</a></td> \n\
        <td><a data-address-id ='" + number + "' data-toggle='modal' data-target='#showEditModal'>Edit</a></td>\n\
         </tr>";
    }

    function buildAddressShowEditRow(data, number) {
        number = number + 1;
        return "<tr class = 'address-view-edit-row'> \n\
         <th width = '30%'>Address " + number + "</th> \n\
         <td width = '70%'>" + data.street.streetNumber + " " + data.street.streetName + ", " + data.location.city + ", " + data.location.state + ", " + data.location.zip + "</td>\n\
         </tr>\n\
         <tr class = 'address-view-edit-row'> \n\
         <th></th>\n\
         <td><a data-address-id = '" + data.addressId + "' class = 'editAddressLink' >Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;<a data-address-id = '" + data.addressId + "' class = 'deleteAddressLink'>Delete</a></td>\n\
         </tr>";
    }


});