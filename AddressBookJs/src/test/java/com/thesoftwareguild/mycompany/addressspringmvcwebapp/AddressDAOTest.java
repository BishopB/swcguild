//package com.thesoftwareguild.mycompany.addressspringmvcwebapp;
//
//import com.thesoftwareguild.mycompany.addressspringmvcwebapp.dao.AddressBookDao;
//import com.thesoftwareguild.mycompany.addressspringmvcwebapp.model.Address;
//import java.util.Arrays;
//import java.util.List;
//import junit.framework.Assert;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
///**
// *
// * @author apprentice
// */
//public class AddressDAOTest {
//
//    AddressBookDao dao = new AddressDaoLambda(true);
//
//    Address third;
//    Address second;
//    Address first;
//
//    public AddressDAOTest() {
//    }
//
//    @Before
//    public void setUp() {
////         Address first = first;
////        Address second = new Address("Pat", "Toner", "123 Bad Wolf Ln.", "Jhoto", "Ohio", "1337", "UK", 2);
////        dao.create(third);
//        first = new Address();
//        first.setFirstName("Kyle");
//        first.setLastName("Sickels");
//        first.setStreetNumber("123");
//        first.setStreetName("Bad Wolf Ln.");
//        first.setCity("Kanto");
//        first.setState("Ohio");
//        first.setZip("1337");
//        first.setId(0);
//
//        second = new Address();
//        second.setFirstName("Pat");
//        second.setLastName("Toner");
//        second.setStreetNumber("123");
//        second.setStreetName("Bad Wolf Ln.");
//        second.setCity("Jhoto");
//        second.setState("Ohio");
//        second.setZip("1337");
//        second.setId(2);
//
//        third = new Address();
//        third.setFirstName("Heidi");
//        third.setLastName("Sickels");
//        third.setStreetNumber("5678");
//        third.setStreetName("Something St.");
//        third.setCity("Hoenn");
//        third.setState("Michigan");
//        third.setZip("55555");
//        third.setId(1);
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    //GET_KEYS[]
//    @Test
//    public void testGetkeys3() {
//
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        String[] testkeys;
//        testkeys = dao.list().stream().map(addr -> String.valueOf(addr.getId())).toArray(String[]::new);
//
//        boolean result = true;
//        int i = 0;
//        for (Address index : dao.list()) {
//            if (index.getId() != Integer.parseInt(testkeys[i])) {
//                result = false;
//            }
//            i++;
//        }
//
//        Assert.assertTrue(result);
//    }
//
//    @Test
//    public void testGetkeys1() {
//
//        dao.create(second);
//        String[] testkeys;
//        testkeys = dao.list().stream().map(addr -> String.valueOf(addr.getId())).toArray(String[]::new);
//
//        boolean result = true;
//        int i = 0;
//        for (Address index : dao.list()) {
//            if (index.getId() != Integer.parseInt(testkeys[i])) {
//                result = false;
//            }
//            i++;
//        }
//
//        Assert.assertTrue(result);
//    }
//
//    @Test
//    public void testGetkeys0() {
//        String[] testkeys;
//        testkeys = dao.list().stream().map(addr -> String.valueOf(addr.getId())).toArray(String[]::new);
//
//        boolean result = true;
//        int i = 0;
//        for (Address index : dao.list()) {
//            if (index.getId() != Integer.parseInt(testkeys[i])) {
//                result = false;
//            }
//            i++;
//        }
//
//        Assert.assertTrue(result);
//    }
//
//    //GET_ADDRESS_BY_LASTNAME
//    @Test
//    public void getAddressesByLastName2() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        List<Address> result = dao.searchByLastName("Sickels");
//
//        Assert.assertEquals(2, result.size());
//    }
//
//    @Test
//    public void getAddressesByLastName1() {
//        dao.create(first);
//
//        List<Address> result = dao.searchByLastName("Sickels");
//
//        Assert.assertEquals(1, result.size());
//    }
//
//    @Test
//    public void getAddressesByLastName0() {
//
//        List<Address> result = dao.searchByLastName("Sickels");
//
//        Assert.assertEquals(0, result.size());
//    }
//
//    //UPDATE_ADDRESS
//    @Test
//    public void updateAddress2() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        Address firstAmend = first;
//        firstAmend.setCity("Vermillion City");
//        
//        dao.update(firstAmend);
//
//        Address secondAmend = second;
//        secondAmend.setFirstName("Random");
//        
//        dao.update(secondAmend);
//        
//        Assert.assertEquals("Random", dao.get(2).getFirstName());
//        Assert.assertEquals("Vermillion City", dao.get(0).getCity());
//        
//    }
//
//    @Test
//    public void updateAddress1() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        Address secondAmend = second;
//        secondAmend.setFirstName("Random");
//        
//        dao.update(secondAmend);
//       
//        Assert.assertEquals("Random", dao.get(2).getFirstName());
//    }
//
//    @Test
//    public void updateAddress0() {
//        dao.update(null);
//
//        boolean result;
//        if (dao.list().size() == 0) {
//            result = true;
//        } else {
//            result = false;
//        }
//
//        Assert.assertTrue(result);
//    }
//
//    //CREATE_ADDRESS
//    @Test
//    public void create3() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        int result = dao.list().size();
//
//        Assert.assertEquals(3, result);
//    }
//
//    @Test
//    public void create1() {
//        dao.create(first);
//
//        int result = dao.list().size();
//
//        Assert.assertEquals(1, result);
//    }
//
//    @Test
//    public void create0() {
//        int result = dao.list().size();
//
//        Assert.assertEquals(0, result);
//    }
//
//    //DELETE_ADDRESS
//    @Test
//    public void deleteAddress3() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        dao.delete(0);
//        dao.delete(1);
//        dao.delete(2);
//
//        int result = dao.list().size();
//
//        Assert.assertEquals(0, result);
//    }
//
//    @Test
//    public void deleteAddress1() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        dao.delete(0);
//
//        int result = dao.list().size();
//
//        Assert.assertEquals(2, result);
//    }
//
//    @Test
//    public void deleteAddress0() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        //not an id of an index
//        dao.delete(7);
//
//        int result = dao.list().size();
//
//        Assert.assertEquals(3, result);
//    }
//
//    //GET_LIST
//    @Test
//    public void list3() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        boolean result = false;
//
//        boolean result1 = false;
//        boolean result2 = false;
//        boolean result3 = false;
//
//        for (Address index : dao.list()) {
//            if (index.getId() == 0) {
//                result1 = true;
//            } else if (index.getId() == 1) {
//                result2 = true;
//            } else if (index.getId() == 2) {
//                result3 = true;
//            }
//        }
//        if (result1 && result2 && result3) {
//            result = true;
//        }
//
//        Assert.assertTrue(result);
//    }
//
//    @Test
//    public void list2() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        boolean result = false;
//
//        boolean result1 = false;
//        boolean result2 = false;
//        boolean result3 = false;
//
//        for (Address index : dao.list()) {
//            if (index.getId() == 0) {
//                result1 = true;
//            } else if (index.getId() == 1) {
//                result2 = true;
//            } else if (index.getId() == 5) {
//                result3 = true;
//            }
//        }
//        if (result1 && result2 && result3) {
//            result = true;
//        }
//
//        Assert.assertFalse(result);
//    }
//
//    @Test
//    public void list1() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        boolean result = false;
//
//        boolean result1 = false;
//        boolean result2 = false;
//        boolean result3 = false;
//
//        for (Address index : dao.list()) {
//            if (index.getId() == 0) {
//                result1 = true;
//            }
//        }
//        if (result1 && result2 && result3) {
//            result = true;
//        }
//
//        Assert.assertFalse(result);
//    }
//
//    @Test
//    public void searchAddressesByZip2() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        List<Address> result = dao.searchByZip("1337");
//
//        Assert.assertEquals(2, result.size());
//    }
//
//    @Test
//    public void searchAddressesByZip1() {
//        dao.create(first);
//
//        List<Address> result = dao.searchByZip("1337");
//
//        Assert.assertEquals(1, result.size());
//    }
//
//    @Test
//    public void searchAddressesByZip0() {
//
//        List<Address> result = dao.searchByZip("1337");
//
//        Assert.assertEquals(0, result.size());
//    }
//
//    @Test
//    public void searchAddressesByCity2() {
//        second.setCity("Kanto");
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        List<Address> result = dao.searchByCity("Kanto");
//
//        Assert.assertEquals(2, result.size());
//    }
//
//    @Test
//    public void searchAddressesByCity1() {
//        dao.create(first);
//
//        List<Address> result = dao.searchByCity("Kanto");
//
//        Assert.assertEquals(1, result.size());
//    }
//
//    @Test
//    public void searchAddressesByCity0() {
//
//        List<Address> result = dao.searchByCity("Kanto");
//
//        Assert.assertEquals(0, result.size());
//    }
//
//    @Test
//    public void searchAddressesByState2() {
//        dao.create(first);
//        dao.create(third);
//        dao.create(second);
//
//        List<Address> result = dao.searchByState("Ohio");
//
//        String firstResult = result.get(0).getFirstName();
//        String secondResult = result.get(1).getFirstName();
//        String[] resultListToBe = new String[]{secondResult, firstResult};
//        List<String> orderedResult = Arrays.asList(resultListToBe);
//
//        String[] listToBe = new String[]{first.getFirstName(), second.getFirstName()};
//        List<String> expected = Arrays.asList(listToBe);
//        Assert.assertEquals(2, result.size());
//        Assert.assertEquals(expected, orderedResult);
//    }
//
//    @Test
//    public void searchAddressesByState1() {
//        dao.create(first);
//
//        List<Address> result = dao.searchByState("Ohio");
//
//        Assert.assertEquals(1, result.size());
//    }
//
//    @Test
//    public void searchAddressesByState0() {
//
//        List<Address> result = dao.searchByState("Ohio");
//
//        Assert.assertEquals(0, result.size());
//    }
//    //ENCODE
//    /*???*/
//    //DECODE
//    /*???*/
//}
