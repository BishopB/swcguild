<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class ="container">
            <jsp:include page="navbar.jsp" />
            <div class="row">
                <table class="table">
                    <tr>
                        <th style="text-align: center; color: #6633ff;">${address.firstName} ${address.lastName}</th>
                    </tr>
                    <tr>
                        <td align = "center"> <span style="color: green;">First Name:</span> ${address.firstName}</td>
                    </tr>
                    <tr>    
                        <td align = "center"><span style="color: green;">Last Name:</span> ${address.lastName}</td>
                    </tr>
                    <tr>
                        <td align = "center"><span style="color: green;">Address:</span> ${address.streetNumber} ${address.streetName}</td>
                    </tr>
                    <tr>
                        <td align = "center"><span style="color: green;">City: </span>${address.city}</td>
                    </tr>
                    <tr>
                        <td align = "center"><span style="color: green;">State: </span>${address.state}</td>
                    </tr>
                    <tr>
                        <td align = "center"><span style="color: green;">Zip Code:</span> ${address.zip}</td>
                    </tr>
                </table>
            </div>
            <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
