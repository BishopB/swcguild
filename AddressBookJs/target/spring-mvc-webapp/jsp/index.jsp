<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .errorMsg {
                color: red;
                text-align: left;
                font-style: italic;
            }
        </style>
    </head>

    <body>
        <div class ="container">
            <jsp:include page="navbar.jsp" />
            <div class="row">
                <div class ="col-md-4 col-md-push-1">
                    <h2>Address List</h2>
                    <table id="addressTable" class="table">
                        <tr>
                            <th width="40%">Last Name, First Name</th>
                            <th width="20%"></th>
                            <th width="20%"></th>
                        </tr>
                        <c:forEach items="${addressList}" var="address">
                            <tr id="address-row-${address.contactId}">
                                <td><a data-address-id ="${address.contactId}" data-toggle="modal" data-target="#showAddressModal">${address.lastName}, ${address.firstName} </a></td>
                                <td><a data-address-id="${address.contactId}" data-toggle="modal" data-target = "#showEditModal">Edit</a></td>
                                <td><a data-address-id="${address.contactId}" class="delete-link">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class ="col-md-6 col-md-push-1">
                    <h2>Add New Address</h2>

                    <form id = "create-address-form" class ="form-horizontal" >

                        <div class="form-group">
                            <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-firstName" class ="errorMsg"></div>
                                <input type="text" name="firstName" class="form-control" id="add-first-name" placeholder="First Name" />

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-lastName" class ="errorMsg"></div>
                                <input type="text" name="lastName" class="form-control" id="add-last-name" placeholder="Last Name" />

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-street-number" class="col-md-4 control-label">Street Number:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-streetNumber" class ="errorMsg"></div>
                                <input type="text" name="streetNumber" class="form-control" id="add-street-number" placeholder="Street Number" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-street-name" class="col-md-4 control-label">Street Name:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-streetName" class ="errorMsg"></div>
                                <input type="text" name="streetName" class="form-control" id="add-street-name" placeholder="Street Name" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-city" class="col-md-4 control-label">City:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-city" class ="errorMsg"></div>
                                <input type="text" name="city" class="form-control" id="add-city" placeholder="City" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-state" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-state" class ="errorMsg"></div>
                                <input type="text" name="state" class="form-control" id="add-state" placeholder="State" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-zip" class="col-md-4 control-label">Zip Code:</label>
                            <div class="col-md-8">
                                <div id = "create-entry-error-zip" class ="errorMsg"></div>
                                <input type="text" name="zip" class="form-control" id="add-zip" placeholder="Zip" />

                            </div>
                        </div>

                        <input type="submit" class="btn btn-default pull-right" value="Create"/>
                    </form>
                </div>
            </div>
        </div>

        <div id="showAddressModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!--Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Address Details</h4>
                    </div>
                    <div class ="modal-body">

                        <table id = "show-modal-table" class="table table-bordered">
                            <tr>
                                <th>First Name:</th>
                                <td id="address-first-name"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="address-last-name"></td>
                            </tr>
                            <!--                            <tr>
                                                            <th>Street:</th>
                                                            <td id="address-street-info"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Location:</th>
                                                            <td id="address-location-info"></td>
                                                        </tr>-->
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        
            
        <div id="editActualAddressModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Address</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            
                            <tr>
                                <th>Street Number:</th>
                                <td>
                                    <input type="text" id="edit-street-number" />
                                </td>
                            </tr>
                            <tr>
                                <th>Street Name:</th>
                                <td>
                                    <input type="text" id="edit-street-name" />
                                </td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td>
                                    <input type="text" id="edit-city" />
                                </td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <input type="text" id="edit-state" />
                                </td>
                            </tr>
                            <tr>
                                <th>Zip Code:</th>
                                <td>
                                    <input type="text" id="edit-zip" />
                                </td>
                            </tr>
                        </table>
                        <!--                        <input type="hidden" id="edit-id" />-->
                        <button id="edit-address-button" class ="btn btn-default" >Edit Address</button>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="errorTeaPot" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!--Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Did You Mean To Do That?</h4>
                    </div>
                    <div class ="modal-body">

                        <table id = "teapot-modal-table" class="table table-bordered">

                        </table>
                        <input type ="hidden" id ="hidden-firstName"/>
                        <input type ="hidden" id ="hidden-lastName"/>
                        <input type ="hidden" id ="hidden-streetNumber"/>
                        <input type ="hidden" id ="hidden-streetName"/>
                        <input type ="hidden" id ="hidden-city"/>
                        <input type ="hidden" id ="hidden-state"/>
                        <input type ="hidden" id ="hidden-zip"/>
                    </div>
                    <div class="modal-footer">
                        <button id ="duplicate" type =" button" class ="btn btn-default" data-dismiss="modal">YES</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="showEditModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!--Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id ="show-edit-view-header" class="modal-title"></h4>
                    </div>
                    <div class ="modal-body">

                        <table id = "edit-choice-modal-table" class="table table-bordered">

                        </table>
                        
                    </div>
                    <div class="modal-footer">
                        <button id ="addressAddition" type =" button" class ="btn btn-default" data-dismiss="modal">Add Another Address</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var contextRoot = '${pageContext.request.contextPath}';
        </script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
    </body>
</html>
