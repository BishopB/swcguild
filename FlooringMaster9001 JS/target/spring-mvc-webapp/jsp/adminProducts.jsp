<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .theme1 {
                background-color: ${rct[0]};
            }
            .theme2 {
                background-color: ${rct[1]};
            }
            .white {
                background-color: white;
                border-radius: 3px;
            }
            .btn-panel {
                width: 300px;
                height: 400px;
                vertical-align: middle;
            }
            .btn-batcave {
                width: 128px;
                color: whitesmoke;
                background-size: 128px 32px;
                background-image: url(../img/BatLogo.png);
            }
            .btn-batcave:hover {
                font-weight: bold;
                color: lightgoldenrodyellow;
            }
            .content-pane{
                width: 360px;
                height: 400px;
            }
            .scollz-down {
                overflow-y: scroll;
            }
            .errorMsg {
                color: red;
                text-align: left;
                font-style: italic;
            }
        </style>
    </head>

    <body class="theme2">
        <div class ="container theme1">
            <div class="row">
                <div class ="col-md-6 col-md-push-3">
                    <h1>Flooring Mastery 9001 <small>Admin - Batcave</small></h1>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-push-1">
                    
                    <form id="search-product-form" class="form-horizontal">
                        <!-- search -->
                        <div class="input-group">
                            <input id="searchValue" type="text" class="form-control" name="searchValue" placeholder="Search for..." value="${searchValue.length()>0 ? searchValue : ''}">
                            <span class="input-group-btn">
                                <button id="search-product-btn" class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </span>
                        </div>
                    </form>


                    <h2 class="white">Products <small>${searchValue.length()>0 ? 'that contain "'+=searchValue+='"' : ''}</small></h2>
                    <c:choose>
                        <c:when test="${allTheProducts.size()>0}">
                            <div class="content-pane white ${allTheProducts.size()>9 ? 'scollz-down' : ''}">
                                <table id="productTable" class="table">
                                    <tr>
                                        <th width="60%" class="text-center">Products - Material/Labor Cost(Sq Ft)</th>
                                        <th width="20%"></th>
                                        <th width="20%"></th>
                                    </tr>
                                    <c:forEach items="${allTheProducts}" var="product">
                                        <tr id='product-row-${product.id}'>
                                            <td><a data-product-id='${product.id}' data-toggle='modal' data-target='#showProductModal'>${product.productType} - $${product.costSqFt}/$${product.laborCostSqFt}</a></td>
                                            <td><a data-product-id='${product.id}' data-toggle='modal' data-target='#editProductModal'>Edit</a></td>
                                            <td><a data-product-id='${product.id}' class='delete-link'>Delete</a></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="search-results white">
                                <h4><small>No matches were found</small></h4>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-4 col-md-push-3">
                    <div class="white" style="width: 100%; height: 35px">                      
                    </div>
                    <div class="white">
                        <h2 style="margin: 20px 0px 10px 0px">Add Product</h2>
                    </div>
                    <div class="white content-pane">
                        <form id="create-form-product" class ="form-horizontal" >
                            <div id="add-product-validation-errors"></div>

                            <div class="form-group" style="padding: 10px">
                                <label for="add-productType" class="col-md-4 control-label">Product Name:</label>
                                <div class="col-md-8">
                                    <div id="add-product-validation-errors-productType" class="errorMsg"></div>
                                    <input type="text" name="productType" class="form-control" id="add-productType" placeholder="Product Name" />
                                </div>
                            </div>

                            <div class="form-group" style="padding: 10px">
                                <label for="add-costSqFt" class="col-md-4 control-label">Cost per Sq/Ft:</label>
                                <div class="col-md-8">
                                    <div id="add-product-validation-errors-costSqFt" class="errorMsg"></div>
                                    <input type="text" name="costSqFt" class="form-control" id="add-costSqFt" placeholder="Cost per Sq/Ft" />
                                </div>
                            </div>

                            <div class="form-group" style="padding: 10px">
                                <label for="add-laborCostSqFt" class="col-md-4 control-label">Labor Cost per Sq/Ft:</label>
                                <div class="col-md-8">
                                    <div id="add-product-validation-errors-laborCostSqFt" class="errorMsg"></div>
                                    <input type="text" name="laborCostSqFt" class="form-control" id="add-laborCostSqFt" placeholder="Labor Cost per Sq/Ft" />
                                </div>
                            </div> 

                            <input type="hidden" name="searchValue" value="${searchValue.length()>0 ? searchValue : ''}">

                            <input style="margin-top: 10px" type="submit" class="btn btn-default" value="Create"/>

                        </form>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 25px">
                <div class="col-md-8 col-md-push-2 text-center">
                    <a href="${pageContext.request.contextPath}/" class="btn btn-default btn-batcave">Leave Batcave</a>
                    <a href="${pageContext.request.contextPath}/admin/choose" class="btn btn-default btn-batcave">Return</a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="showProductModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Product Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <tr>
                                <th>Product Name:</th>
                                <td id="product-productType"></td>
                            </tr>
                            <tr>
                                <th>Product Material Cost per Sq Ft:</th>
                                <td id="product-costSqFt"></td>
                            </tr>
                            <tr>
                                <th>Product Labor Cost per Sq Ft:</th>
                                <td id="product-laborCostSqFt"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div><!--End Modal-->

        <!-- Modal -->
        <div id="editProductModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Product</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Product type:</th>
                                <td>
                                    <div id="edit-product-validation-errors-productType" class="errorMsg"></div>
                                    <input type="text" id="edit-productType" />
                                </td>
                            </tr>
                            <tr>
                                <th>Product Material Cost per Sq Ft:</th>
                                <td>
                                    <div id="edit-product-validation-errors-costSqFt" class="errorMsg"></div>
                                    <input type="text" id="edit-costSqFt" />
                                </td>
                            </tr>
                            <tr>
                                <th>Product Labor Cost per Sq Ft:</th>
                                <td>
                                    <div id="edit-product-validation-errors-laborCostSqFt" class="errorMsg"></div>
                                    <input type="text" id="edit-laborCostSqFt" />
                                </td>
                            </tr>
                        </table>

                        <input type="hidden" id="edit-id" />
                        <button id="edit-product-button" class="btn btn-default">Edit Product</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div><!--End Modal-->

        <script type="text/javascript">
            var contextRoot = '${pageContext.request.contextPath}';
        </script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/adminApp.js"></script>
    </body>
</html>