$(document).ready(function () {

    $(document).on('click', '#search-product-btn', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: contextRoot + '/admin/product/search/' + $('#searchValue').val(),
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (data, status) {
            //clear table except first row with headers
            $('#productTable').find("tr:gt(0)").remove();

            //print results
            $.each(data, function (index, Product) {
                $('#productTable').append(buildProductRow(Product));
            });

            //No results
            if ($(data).size() === 0) {
                $('#productTable').append(buildNoResultsRow());
            }
        });
    });//END search product

    $(document).on('submit', '#create-form-product', function (e) {
        e.preventDefault();
        $('.errorMsg').empty();

        var productData = JSON.stringify({
            productType: $('#add-productType').val(),
            costSqFt: $('#add-costSqFt').val(),
            laborCostSqFt: $('#add-laborCostSqFt').val()
        });
        $.ajax({
            type: 'POST',
            url: contextRoot + '/admin/product',
            data: productData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#add-productType').val('');
            $('#add-costSqFt').val('');
            $('#add-laborCostSqFt').val('');

            var tableRow = buildProductRow(data);
            $('#productTable').append($(tableRow));
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#add-product-validation-errors-' + validationError.fieldName).append("* " + validationError.message).append("<br />");
            });
        });
    });

    $('#showProductModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var productId = link.data('product-id');
        var modal = $(this);
        $.ajax({
            type: 'Get',
            url: contextRoot + '/admin/product/' + productId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (product, status) {
            modal.find('#product-productType').text(product.productType);
            modal.find('#product-costSqFt').text(product.costSqFt);
            modal.find('#product-laborCostSqFt').text(product.laborCostSqFt);
        });
    }); //End showProductModal

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var productId = $(e.target).data('product-id');
        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/admin/product/' + productId
        }).success(function (data, status) {
            $('#product-row-' + productId).remove();
        });
    });

    $('#editProductModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var productId = link.data('product-id');
        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + "/admin/product/" + productId,
            dataType: 'json',
            beforeSend: function (xhr) {                             //xhr is an arbitrary name
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (product, status) {
            modal.find('#edit-productType').val(product.productType);
            modal.find('#edit-costSqFt').val(product.costSqFt);
            modal.find('#edit-laborCostSqFt').val(product.laborCostSqFt);

            $('#edit-id').val(productId);
        });
    });
    $(document).on('click', '#edit-product-button', function (e) {
        e.preventDefault();
        $('.errorMsg').empty();

        var productId = $('#edit-id').val();

        $.ajax({
            type: 'PUT',
            url: contextRoot + '/admin/product/' + productId,
            data: JSON.stringify({
                id: productId,
                productType: $('#edit-productType').val(),
                costSqFt: $('#edit-costSqFt').val(),
                laborCostSqFt: $('#edit-laborCostSqFt').val()
            }),
            dataType: 'json',
            beforeSend: function (xhr) {                             //xhr is an arbitrary name
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#editProductModal').modal('hide');
            var tableRow = buildProductRow(data);
            $('#product-row-' + data.id).replaceWith($(tableRow));
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#edit-product-validation-errors-' + validationError.fieldName).append("* " + validationError.message).append("<br />");
            });
        });
    });

    function buildProductRow(data) {
        return "<tr id='product-row-" + data.id + "'> \n\
                    <td><a data-product-id='" + data.id + "' data-toggle='modal' data-target='#showProductModal'>" + data.productType + " - $" + data.costSqFt + "/$" + data.laborCostSqFt + "</a></td>   \n\
                    <td><a data-product-id='" + data.id + "' data-toggle='modal' data-target='#editProductModal'>Edit</a></td>   \n\
                    <td><a data-product-id='" + data.id + "' class='delete-link'>Delete</a></td>   \n\
                </tr>";
    }

//===============================================================

    $(document).on('submit', '#create-form-state', function (e) {
        e.preventDefault();
        $('.errorMsg').empty();

        var stateData = JSON.stringify({
            state: $('#add-state').val(),
            taxRate: parseFloat($('#add-taxRate').val())
        });
        $.ajax({
            type: 'POST',
            url: contextRoot + '/admin/state',
            data: stateData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#add-state').val('');
            $('#add-taxRate').val('');
            var tableRow = buildContentRow(data);
            $('#statesTable').append($(tableRow));
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#add-state-validation-errors-' + validationError.fieldName).append("* " + validationError.message).append("<br />");
            });
        });
    });
    $('#showStateModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var stateId = link.data('state-id');
        var modal = $(this);
        $.ajax({
            type: 'Get',
            url: contextRoot + '/admin/state/' + stateId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (state, status) {
            modal.find('#state-state').text(state.state);
            modal.find('#state-taxRate').text(state.taxRate);
        });
    }); //End showStateModal

    $('#editStateModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var stateId = link.data('state-id');
        var modal = $(this); //#showContactModal

        $.ajax({
            type: 'GET',
            url: contextRoot + "/admin/state/" + stateId,
            dataType: 'json',
            beforeSend: function (xhr) {                             //xhr is an arbitrary name
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (state, status) {
            modal.find('#edit-state').val(state.state);
            modal.find('#edit-taxRate').val(state.taxRate);
            $('#edit-id').val(stateId);
        });
    });
    $(document).on('click', '#edit-state-button', function (e) {
        e.preventDefault();
        $('.errorMsg').empty();

        var stateId = $('#edit-id').val();

        $.ajax({
            type: 'PUT',
            url: contextRoot + '/admin/state/' + stateId,
            data: JSON.stringify({
                id: stateId,
                state: $('#edit-state').val(),
                taxRate: parseFloat($('#edit-taxRate').val())
            }),
            dataType: 'json',
            beforeSend: function (xhr) {                             //xhr is an arbitrary name
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#editStateModal').modal('hide');
            var tableRow = buildContentRow(data);
            $('#state-row-' + data.id).replaceWith($(tableRow));
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#edit-state-validation-errors-' + validationError.fieldName).append("* " + validationError.message).append("<br />");
            });
        });
    });

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var stateId = $(e.target).data('state-id');
        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/admin/state/' + stateId
        }).success(function (data, status) {
            $('#state-row-' + stateId).remove();
        });
    });

    $(document).on('click', '#search-state-btn', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: contextRoot + '/admin/state/search/' + $('#searchValue').val(),
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (data, status) {
            //clear table except first row with headers
            $('#statesTable').find("tr:gt(0)").remove();

            //print results
            $.each(data, function (index, States) {
                $('#statesTable').append(buildContentRow(States));
            });

            //No results
            if ($(data).size() === 0) {
                $('#statesTable').append(buildNoResultsRow());
            }
        });
    });//END search states

    function buildContentRow(data) {
        return "<tr id='state-row-" + data.id + "'>  \n\
                    <td><a data-state-id='" + data.id + "' data-toggle='modal' data-target='#showStateModal'>" + data.state + " - " + data.taxRate + "</a></td> \n\
                    <td><a data-state-id='" + data.id + "' data-toggle='modal' data-target='#editStateModal'>Edit</a></td> \n\
                    <td><a data-state-id='" + data.id + "' class='delete-link'>Delete</a></td>   \n\
                </tr>";
    }

    function buildNoResultsRow() {
        return "<tr id='no-result-row' class='emptyResults'>  \n\
                    <td colspan='3'><h3><small>No Results</small></h3></td> \n\
                </tr>";
    }

}); //End document