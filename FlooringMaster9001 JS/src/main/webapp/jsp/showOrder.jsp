<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

        <style>
            h2{
                text-align: center;
            }
            .theme1 {
                background-color: ${rct[0]};
                width: 95%;
                margin: 15px auto;

            }
            .theme2 {
                background-color: ${rct[1]};
            }
            .white {
                background-color: white;

            }
            
        </style>

    </head>

    <body class = "theme2">
        <div class ="container ">
            <div class="row theme1 " style="padding: 15px;"> 


                <div class ="col-md-12 white">
                    <h2>View Order</h2>
                    <form:form class ="form-horizontal" id="orderViewForm" modelAttribute="order">
                        
                        <div class="form-group">
                            <label for="add-date" class="col-md-4 control-label">Date:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-date" value="<c:out value="${order.fileName.substring(7,15)}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-name" value="<c:out value="${order.name}"/>" readonly/>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for ="add-productType" class="col-md-4 control-label">Product:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-productType" value="<c:out value="${order.productType}"/>" readonly/>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for ="add-laborCostSqFt" class="col-md-4 control-label">Product Cost Per Sq Ft:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-laborCostSqFt" value="<c:out value="${order.laborCostSqFt}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for = "add-state" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-state" value="<c:out value="${order.state}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for ="add-taxRate" class="col-md-4 control-label">State Tax Rate:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-taxRate" value="<c:out value="${order.taxRate}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-area" class="col-md-4 control-label">Area:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-area" value="<c:out value="${order.area}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for ="add-materialCost" class="col-md-4 control-label">Material Cost:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-materialCost" value="<c:out value="${order.materialCost}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for ="add-laborCost" class="col-md-4 control-label">Labor Cost:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-laborCost" value="<c:out value="${order.laborCost}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for ="add-taxCost" class="col-md-4 control-label">Total Tax:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-taxCost" value="<c:out value="${order.taxCost}"/>" readonly/>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for ="add-totalCost" class="col-md-4 control-label">Total Cost:</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" id="add-totalCost" value="<c:out value="${order.totalCost}"/>" readonly/>

                            </div>
                        </div>



                    </form:form>
                </div>

            </div>
            <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
