<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .theme1 {
                background-color: ${rct[0]};
            }
            .theme2 {
                background-color: ${rct[1]};
            }
            .white {
                background-color: white;
                border-radius: 3px;
            }
            .btn-panel {
                width: 300px;
                height: 400px;
                vertical-align: middle;
            }
            .btn-batcave {
                width: 128px;
                color: whitesmoke;
                background-size: 128px 32px;
                background-image: url(../img/BatLogo.png);
            }
            .btn-batcave:hover {
                font-weight: bold;
                color: lightgoldenrodyellow;
            }
            .content-pane{
                width: 360px;
                height: 400px;
            }
            .scollz-down {
                overflow-y: scroll;
            }
            .errorMsg {
                color: red;
                text-align: left;
                font-style: italic;
            }
        </style>
    </head>

    <body class="theme2">
        <div class ="container theme1">
            <div class="row">
                <div class ="col-md-6 col-md-push-3">
                    <h1>Flooring Mastery 9001 <small>Admin - Batcave</small></h1>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-push-1">
                    
                    <form id="search-state-form" class="form-horizontal">
                        <!-- search -->
                        <div class="input-group">
                            <input id="searchValue" type="text" class="form-control" name="searchValue" placeholder="Search for..." value="${searchValue.length()>0 ? searchValue : ''}">
                            <span class="input-group-btn">
                                <button id="search-state-btn" class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            </span>
                        </div>
                    </form>


                    <h2 class="white">States <small>${searchValue.length()>0 ? 'that contain "'+=searchValue+='"' : ''}</small></h2>
                    <c:choose>
                        <c:when test="${allTheStates.size()>0}">
                            <div class="content-pane white ${allTheStates.size()>9 ? 'scollz-down' : ''}">
                                <table id="statesTable" class="table">
                                    <tr>
                                        <th width="40%" class="text-center">State - Tax Rate</th>
                                        <th width="30%"></th>
                                        <th width="30%"></th>
                                    </tr>
                                    <c:forEach items="${allTheStates}" var="state">
                                        <tr id='state-row-${state.id}'>  
                                            <td><a data-state-id='${state.id}' data-toggle='modal' data-target='#showStateModal'>${state.state} - ${state.taxRate}</a></td> 
                                            <td><a data-state-id='${state.id}' data-toggle='modal' data-target='#editStateModal'>Edit</a></td> 
                                            <td><a data-state-id='${state.id}' class='delete-link'>Delete</a></td>   
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="search-results white">
                                <h4><small>No matches were found</small></h4>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-4 col-md-push-3">
                    <div class="white" style="width: 100%; height: 35px">                      
                    </div>
                    <div class="white">
                        <h2 style="margin: 20px 0px 10px 0px">Add State</h2>
                    </div>
                    <div class="white content-pane">

                        <form id="create-form-state" class ="form-horizontal" >
                            <div class="form-group" style="padding: 10px">
                                <label for="add-state" class="col-md-4 control-label">State Name:</label>
                                <div class="col-md-8">
                                    <div id="add-state-validation-errors-state" class="errorMsg"></div>
                                    <input type="text" name="state" class="form-control" id="add-state" placeholder="State Name"/>
                                </div>
                            </div>

                            <div class="form-group" style="padding: 10px">
                                <label for="add-taxRate" class="col-md-4 control-label">Tax Rate:</label>
                                <div class="col-md-8">
                                    <div id="add-state-validation-errors-taxRate" class="errorMsg"></div>
                                    <input type="text" name="taxRate" class="form-control" id="add-taxRate" placeholder="Tax Rate"/>
                                </div>
                            </div>

                            
                            
                            
                            
                            <input style="margin-top: 10px" type="submit" class="btn btn-default" value="Create State"/>
                        </form>

                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 25px">
                <div class="col-md-8 col-md-push-2 text-center">
                    <a href="${pageContext.request.contextPath}/" class="btn btn-default btn-batcave">Leave Batcave</a>
                    <a href="${pageContext.request.contextPath}/admin/choose" class="btn btn-default btn-batcave">Return</a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="showStateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">State Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <tr>
                                <th>State Abbreviation:</th>
                                <td id="state-state"></td>
                            </tr>
                            <tr>
                                <th>State Tax Rate:</th>
                                <td id="state-taxRate"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div><!--End Modal-->

        <!-- Modal -->
        <div id="editStateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit State</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>State Abbreviation:</th>
                                <td>
                                    <div id="edit-state-validation-errors-state" class="errorMsg"></div>
                                    <input type="text" id="edit-state" />
                                </td>
                            </tr>
                            <tr>
                                <th>State Tax Rate</th>
                                <td>
                                    <div id="edit-state-validation-errors-taxRate" class="errorMsg"></div>
                                    <input type="text" id="edit-taxRate" />
                                </td>
                            </tr>
                        </table>

                        <input type="hidden" id="edit-id" />
                        <button id="edit-state-button" class="btn btn-default">Edit State</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div><!--End Modal-->

        <input type="hidden" id="states-list" var="statesList" value="${allTheStates}"/>
        <script type="text/javascript">
            var contextRoot = '${pageContext.request.contextPath}';
        </script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/adminApp.js"></script>
    </body>
</html>