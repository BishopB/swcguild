/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.models;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Product {

    private Integer id;
    @NotEmpty(message = "Required: Product Type")
    private String productType;
    @NotNull(message = "Required: Material Cost per Square Foot")
    @Digits(integer = 5, fraction = 2, message = "Please Enter A Number between 0.01 and 99999.99")
    private Double costSqFt;
    @NotNull(message = "Required: Labor Cost per Square Foot")
    @Digits(integer = 5, fraction = 2, message = "Please Enter A Number between 0.01 and 99999.99")
    private Double laborCostSqFt;

    public Product(Integer id, String productType, double costSqFt, double laborCostSqFt) {
        this.id = id;
        this.productType = productType;
        this.costSqFt = Math.ceil(costSqFt * 100) / 100;
        this.laborCostSqFt = Math.ceil(laborCostSqFt * 100) / 100;
    }
    
    public Product(){
        
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the costSqFt
     */
    public Double getCostSqFt() {
        return costSqFt;
    }

    /**
     * @param costSqFt the costSqFt to set
     */
    public void setCostSqFt(Double costSqFt) {
        this.costSqFt = costSqFt;
    }

    /**
     * @return the laborCostSqFt
     */
    public Double getLaborCostSqFt() {
        return laborCostSqFt;
    }

    /**
     * @param laborCostSqFt the laborCostSqFt to set
     */
    public void setLaborCostSqFt(Double laborCostSqFt) {
        this.laborCostSqFt = laborCostSqFt;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
