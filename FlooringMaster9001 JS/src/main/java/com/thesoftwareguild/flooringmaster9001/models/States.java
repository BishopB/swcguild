/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.models;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class States {

    private Integer id;
    
    @Size(min = 2, max = 2)
    @Pattern(regexp = "^[A-Za-z]+$", message = "State abbreviation may contain only letters")
    @NotEmpty(message = "Required: State Name")
    private String state;

    @Digits(integer = 5, fraction = 3, message = "Please Enter A Number between 0.001 and 99999.99")
    @NotNull(message = "Required: Tax Rate")
    private Double taxRate;

    public States(Integer id, String state, double taxRate) {
        this.id = id;
        this.state = state;
        this.taxRate = taxRate;
    }

    public States() {
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @return the taxRate
     */
    public Double getTaxRate() {
        return taxRate;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
