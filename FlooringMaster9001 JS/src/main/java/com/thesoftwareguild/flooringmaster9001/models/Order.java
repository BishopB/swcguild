/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.models;

import java.time.LocalDate;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author apprentice
 */
public class Order {
    //primary key
    private Integer id;
    //foreign keys
    private Integer orderNum;
    @NotNull(message = "Please Choose A State")
    private States state;
    @NotNull(message = "Please Choose A Product")
    private Product product;
    //attributes
    @NotEmpty(message = "Please Enter A Customer Name")
    private String name;
    @Digits(integer=5, fraction = 2, message = "Please Enter A Number between 0.01 and 99999.99")
    @NotNull(message="Please Enter An Area")
    private Double area;
    private Double materialCost;
    private Double laborCost;
    private Double taxCost;
    private Double totalCost;
    @NotNull(message="Please Choose A Date YYYY-MM-DD")
    @DateTimeFormat(pattern= "yyyy-MM-dd")
    private LocalDate date;
 
    public Order(Integer id, Integer orderNum, String name, Double area, 
            Double materialCost, Double laborCost, Double taxCost, Double totalCost, String fileName, LocalDate date){
        this.orderNum = orderNum;
        this.name = name;
        this.area = area;
        this.materialCost = Math.ceil(materialCost*100)/100;
        this.laborCost = Math.ceil(laborCost*100)/100;
        this.taxCost = Math.ceil(taxCost*100)/100;
        this.totalCost = Math.ceil(totalCost*100)/100;
        this.id= id;
        this.date = date;
    }
    
    public Order(){
        
    }
    
    /**
     * @return the orderNum
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * @param orderNum the orderNum to set
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public Double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(Double area) {
        this.area = area;
    }

    public Double getMaterialCost() {
        return materialCost;
    }

   
    public void setMaterialCost(Double materialCost) {
        this.materialCost = Math.ceil(materialCost*100)/100;
    }

   
    public Double getLaborCost() {
        return laborCost;
    }

    
    public void setLaborCost(Double laborCost) {
        this.laborCost = Math.ceil(laborCost*100)/100;
    }

   
    public Double getTaxCost() {
        return taxCost;
    }

  
    public void setTaxCost(Double taxCost) {
        this.taxCost = Math.ceil(taxCost*100)/100;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = Math.ceil(totalCost*100)/100; 
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }
    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }
}