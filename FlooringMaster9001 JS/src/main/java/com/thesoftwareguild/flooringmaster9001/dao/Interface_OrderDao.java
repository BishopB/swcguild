package com.thesoftwareguild.flooringmaster9001.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.thesoftwareguild.flooringmaster9001.models.Order;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface Interface_OrderDao {

    void createOrder(Order currentOrder);

    void deleteFile();

    void deleteOrder(Order currentOrder);

    String getDate(String fileName);

    String getFileName(Order currentOrder);

    List<Order> getFileOrderList(String fileName);

    Integer getNextId(String fileName);

    Order getOrder(Integer orderNum, String fileName);

    String[] getStringOrders(String fileName);

    void setup(boolean testMode);
    
    public List<String> getActiveFiles();

    void testTrigger();

    void updateOrder(Order currentOrder, double laborCostSqFt, double costSqFt, double tax);
    
}
