/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.dao;

import com.thesoftwareguild.flooringmaster9001.encodeDecode.EncodeDecodeOrder;
import com.thesoftwareguild.flooringmaster9001.models.Order;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class OrderDao {

    private static String DELIMETER = ",";
    private boolean testMode;
    private EncodeDecodeOrder encodeDecode;
    private List<Order> orderList = new ArrayList<>();
    private Integer nextId = 0;

    public OrderDao(boolean testMode, EncodeDecodeOrder encodeDecodeBean) {
        this.encodeDecode = encodeDecodeBean;
        setup(testMode);
    }

    public void setup(boolean testMode) {
        File dir = new File(System.getProperty("user.dir"));
        File[] foundFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("Orders_");
            }
        });

        for (File file : foundFiles) {
            orderList.addAll(encodeDecode.decode(file));
        }
        try {
            nextId = 1 + orderList.stream()
                    .max((s1, s2)
                            -> Integer.compare(s1.getId(), s2.getId()))
                    .get()
                    .getId();
        } catch (Exception e) {
            nextId = 0;
        }

        this.testMode = testMode;
    }

    public List<Order> getFileOrderList(String fileName) {
        List<Order> fileOrderList = new ArrayList<>();
        for (Order current : orderList) {
            if (current.getFileName().equals(fileName)) {
                fileOrderList.add(current);
            }
        }
        return fileOrderList;
    }

    public Order createOrder(Order currentOrder) {
        currentOrder.setId(nextId++);
        currentOrder.setOrderNum(getNextId(currentOrder.getFileName()));
        orderList.add(currentOrder);
        encodeDecode.encode(testMode, currentOrder.getFileName(), orderList);
        return currentOrder;
    }

    public Integer getNextId(String fileName) {
        int fileNextId = 1;

        for (Order current : getFileOrderList(fileName)) {
            if (current.getOrderNum() >= fileNextId) {
                fileNextId = current.getOrderNum() + 1;
            }
        }
        return fileNextId;
    }

    public void deleteOrder(Order currentOrder) {
        for (Order current : orderList) {
            if (current.getFileName().equals(currentOrder.getFileName()) && Objects.equals(current.getOrderNum(), currentOrder.getOrderNum())) {
                orderList.remove(current);
                break;
            }
        }
        encodeDecode.encode(testMode, currentOrder.getFileName(), orderList);
        deleteFile();
    }

    public void updateOrder(Order currentOrder, double laborCostSqFt, double costSqFt, double tax) {
        //only works on same date. separate date order changes need to be deleted and orderDao.create(revisedOrder)
        orderList.remove(getOrder(currentOrder.getOrderNum(), currentOrder.getFileName()));
//        currentOrder.setLaborCostSqFt(laborCostSqFt);
//        currentOrder.setCostSqFt(costSqFt);
//        currentOrder.setTaxRate(tax);
//        currentOrder.setMaterialCost(currentOrder.getCostSqFt() * currentOrder.getArea());
//        currentOrder.setLaborCost(currentOrder.getLaborCostSqFt() * currentOrder.getArea());
//        currentOrder.setTaxCost((currentOrder.getTaxRate() / 100.0) * (currentOrder.getLaborCost() + currentOrder.getMaterialCost()));
        currentOrder.setTotalCost(currentOrder.getLaborCost() + currentOrder.getMaterialCost() + currentOrder.getTaxCost());
        orderList.add(currentOrder);
        encodeDecode.encode(testMode, currentOrder.getFileName(), orderList);
        deleteFile();

    }

    public void updateOrder(Order order) {
        Order toRemove = null;
        for (Order current : orderList) {
            if (Objects.equals(current.getId(), order.getId())) {
                toRemove = current;
                break;
            }
        }
        orderList.remove(toRemove);
        order.setOrderNum(getNextId(order.getFileName()));
        orderList.add(order);
        encodeDecode.encode(testMode, order.getFileName(), orderList);
        deleteFile();

    }

    public Order getOrder(Integer orderNum, String fileName) {
        for (Order current : orderList) {
            if (current.getFileName().equals(fileName) && current.getOrderNum() == orderNum) {
                return current;
            }
        }
        return null;
    }

    public Order getOrder(Integer orderId) {
        return orderList.stream()
                .filter(o -> Objects.equals(o.getId(), orderId))
                .findFirst()
                .get();
    }

    public String getFileName(Order currentOrder) {
        return currentOrder.getFileName();
    }

    public String getDate(String fileName) {
        return fileName.substring(7, 9) + "/" + fileName.substring(9, 11) + "/" + fileName.substring(11, 15);
    }

    public String[] getStringOrders(String fileName) {
        String[] orders = new String[getFileOrderList(fileName).size()];
        int i = 0;
        for (Order x : getFileOrderList(fileName)) {
            orders[i] = "" + x.getOrderNum();
            i++;
        }
        return orders;
    }

    public void deleteFile() {
        File dir = new File(System.getProperty("user.dir"));
        File[] foundFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith("Orders_");
            }
        });
        for (File file : foundFiles) {
            String fileName = file.toString().substring(file.toString().length() - 19);

            if (!getActiveFiles().contains(fileName)) {
                try {
                    Files.delete(file.toPath());
                } catch (IOException ex) {
                    Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public List<String> getActiveFiles() {
        List<String> fileList = new ArrayList<>();
        for (Order x : orderList) {
            if (!fileList.contains(x.getFileName())) {
                fileList.add(x.getFileName());
            }
        }
        return fileList;
    }

    public void testTrigger() {
        System.out.println("triggered");
    }

    public List<Order> list() {
        List<Order> copyList = orderList;
        return copyList;
    }
}
