/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.dao;

import com.thesoftwareguild.flooringmaster9001.encodeDecode.EncodeDecodeStates;
import com.thesoftwareguild.flooringmaster9001.models.States;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author apprentice
 */
public final class StatesDao {

    //CHANGE XML TO SUPPORT NEW CONSTRZUCTOR ARG
    final static String FILENAME = "taxRates.txt";
    final static String DELIMETER = ",";

    private List<States> statesList = new ArrayList<>();
    private boolean testMode;
    private EncodeDecodeStates encodeDecode;
    private Integer nextId = 0;

    public StatesDao(boolean testMode, EncodeDecodeStates encodeDecodeBean) {
        this.encodeDecode = encodeDecodeBean;
        setup(testMode);
    }

    public void setup(boolean testMode) {
        statesList = decode();
        nextId = 1 + statesList.stream().max((s1, s2) -> Integer.compare(s1.getId(), s2.getId())).get().getId();
        //list.stream().mapToInt(Integer::intValue).max();
//            final Comparator<Person> comp = (p1, p2) -> Integer.compare( p1.getAge(), p2.getAge());
//    Person oldest = personList.stream()
//                              .max(comp)
//                              .get();
        this.testMode = testMode;
    }

    public String[] getStates() {
        String[] states = new String[statesList.size()];
        int i = 0;
        for (States x : statesList) {
            states[i] = x.getState();
            i++;
        }
        return states;
    }

    public String[] getStatesTaxes() {
        String[] st = new String[statesList.size()];
        int i = 0;
        for (States x : statesList) {
            st[i] = x.getState() + " " + x.getTaxRate() + "%";
            i++;
        }
        return st;
    }

    public double getTax(int id) {
        return statesList.stream().filter(x -> id == x.getId()).findFirst().get().getTaxRate();
    }

    public States getState(int id) {
        return statesList.stream().filter(x -> id == x.getId()).findFirst().get();
    }

    public void removeState(int id) {
        statesList.remove(statesList.stream().filter(x -> id == x.getId()).findFirst().get());
        encode();
    }

    public States createState(States newState) {
        newState.setId(nextId);
        statesList.add(newState);
        nextId++;
        encode();
        return newState;
    }

    public void updateState(States state) {
        removeState(state.getId());
        statesList.add(state);
        encode();
    }

    public void renameState(int id, String newStateName) {
        statesList.stream().filter(x -> id == x.getId()).findFirst().get().setState(newStateName);
        encode();
    }

    public void changeStateTax(int id, double newStateTax) {
        statesList.stream().filter(x -> id == x.getId()).findFirst().get().setTaxRate(newStateTax);
        encode();
    }

    public void encode() {
        getEncodeDecode().encode(testMode, statesList, FILENAME, DELIMETER);
    }

    public List<States> decode() {
        return getEncodeDecode().decode(FILENAME, DELIMETER);
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public EncodeDecodeStates getEncodeDecode() {
        return encodeDecode;
    }

    public void setEncodeDecode(EncodeDecodeStates encodeDecode) {
        this.encodeDecode = encodeDecode;
    }

    public List<States> list() {
        List<States> copyList = statesList;
        Collections.sort(copyList, (States o1, States o2) -> o1.getState().compareToIgnoreCase(o2.getState()));
        return copyList;
    }

    //Legacy pending removal USE "getState(int id)"
    public States getTaxObj(String stateName) {
        return statesList.stream().filter(state -> state.getState().toLowerCase().contains(stateName.toLowerCase())).findFirst().get();
    }

    //Legacy pending removal USE "getTax(int id)"
    public double getTax(String stateName) {
        for (States x : statesList) {
            if (x.getState().equalsIgnoreCase(stateName)) {
                return x.getTaxRate();
            }
        }
        return -1;
    }

    //Legacy pending removal USE "removeState(Intger id)"
    public void removeState(String removeState) {
        for (States thisTax : statesList) {
            if (thisTax.getState().equals(removeState)) {
                statesList.remove(thisTax);
                break;
            }
        }
        encode();
    }

    //Legacy pending removal USE "renameState(int id, String newStateName)"
    public void renameState(String oldStateName, String newStateName) {
        for (States thisTax : statesList) {
            if (thisTax.getState().equals(oldStateName)) {
                thisTax.setState(newStateName);
                break;
            }
        }
        encode();
    }

    //Legacy pending removal USE "changeStateTax(int id, double newStateTax)"
    public void changeStateTax(String stateName, double newStateTax) {
        for (States thisTax : statesList) {
            if (thisTax.getState().equals(stateName)) {
                thisTax.setTaxRate(newStateTax);
                break;
            }
        }
        encode();
    }

    //Legacy
    public void createState(String newStateName, double newTaxRate) {
        statesList.add(new States(nextId, newStateName, newTaxRate));
        nextId++;
        encode();
    }
}
