/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.dao;

import com.thesoftwareguild.flooringmaster9001.encodeDecode.EncodeDecodeProduct;
import com.thesoftwareguild.flooringmaster9001.models.Order;
import com.thesoftwareguild.flooringmaster9001.models.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public final class ProductDao {

    final static String FILENAME = "products.txt";
    final static String DELIMETER = ",";

    private static List<Product> productList = new ArrayList<>();
    private static boolean testMode;
    private EncodeDecodeProduct encodeDecode;/* = new EncodeDecode();*/
    private Integer nextId = 0;

    public ProductDao(boolean testMode, EncodeDecodeProduct encodeDecodeBean) {
        this.encodeDecode = encodeDecodeBean;
        setup(testMode);
    }

    public ProductDao() {
    }

    public void setup(boolean testMode) {
        this.testMode = testMode;
        productList = decode();
        nextId = 1 + productList.stream().max((s1, s2) -> Integer.compare(s1.getId(), s2.getId())).get().getId();
    }

    public String[] getProducts() {
        String[] products = new String[productList.size()];
        int i = 0;
        for (Product x : productList) {
            products[i] = x.getProductType();
            i++;
        }
        return products;
    }

    public String[] getProductsCosts() {
        String[] products = new String[productList.size()];
        int i = 0;
        for (Product x : productList) {
            products[i] = x.getProductType() + " Material: " + x.getCostSqFt() + " Labor: " + x.getLaborCostSqFt();
            i++;
        }
        return products;
    }

    public Product getProduct(int id) {
        return productList.stream().filter(x -> id == x.getId()).findFirst().get();
    }

    public void renameProduct(int id, String newProductName) {
        productList.stream().filter(x -> id == x.getId()).findFirst().get().setProductType(newProductName);
        encode();
    }

    public void changeMaterialCost(int id, double newMaterialCost) {
        productList.stream().filter(x -> id == x.getId()).findFirst().get().setCostSqFt(newMaterialCost);
        encode();
    }

    public void changeLaborCost(int id, double newLaborCost) {
        productList.stream().filter(x -> id == x.getId()).findFirst().get().setLaborCostSqFt(newLaborCost);
        encode();
    }

    public void removeProduct(int id) {
        productList.remove(productList.stream().filter(x -> id == x.getId()).findFirst().get());
        encode();
    }

    public void updateProduct(Product product) {
        removeProduct(product.getId());
        productList.add(product);
        encode();
    }

    public Product createProduct(Product newProduct) {
        newProduct.setId(nextId);
        productList.add(newProduct);
        nextId++;
        encode();
        return newProduct;
    }

    public void encode() {
        encodeDecode.encode(testMode, productList, FILENAME, DELIMETER);
    }

    public List<Product> decode() {
        return encodeDecode.decode(FILENAME, DELIMETER);
    }

    /**
     * @return the encodeDecode
     */
    public EncodeDecodeProduct getEncodeDecode() {
        return encodeDecode;
    }

    /**
     * @param encodeDecode the encodeDecode to set
     */
    public void setEncodeDecode(EncodeDecodeProduct encodeDecode) {
        this.encodeDecode = encodeDecode;
    }

    public List<Product> list() {
        List<Product> copyList = productList;
        return copyList;
    }

    //LEGACY use "getProduct(int id) instead
    public Product getProduct(String productName) {
        for (Product x : productList) {
            if (x.getProductType().equalsIgnoreCase(productName)) {
                return x;
            }
        }
        return (new Product(-1, "null", -1, -1));
    }

    //LEGACY
    public void renameProduct(String oldProductName, String newProductName) {
        for (Product thisProduct : productList) {
            if (thisProduct.getProductType().equals(oldProductName)) {
                thisProduct.setProductType(newProductName);
                break;
            }
        }
        encode();
    }

    //LEGACY
    public void changeMaterialCost(String productName, double newMaterialCost) {
        for (Product thisProduct : productList) {
            if (thisProduct.getProductType().equals(productName)) {
                thisProduct.setCostSqFt(newMaterialCost);
                break;
            }
        }
        encode();
    }

    //LEGACY
    public void changeLaborCost(String productName, double newLaborCost) {
        for (Product thisProduct : productList) {
            if (thisProduct.getProductType().equals(productName)) {
                thisProduct.setLaborCostSqFt(newLaborCost);
                break;
            }
        }
        encode();
    }

    //LEGACY
    public void removeProduct(String productName) {
        for (Product thisProduct : productList) {
            if (thisProduct.getProductType().equals(productName)) {
                productList.remove(thisProduct);
                break;
            }
        }
        encode();
    }

    //Legacy
    public void createProduct(String newProductName, double newMaterialCostSqFt, double newLaborCostSqFt) {
        productList.add(new Product(nextId, newProductName, newMaterialCostSqFt, newLaborCostSqFt));
        nextId++;
        encode();
    }
}
