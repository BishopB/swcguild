/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.encodeDecode;

import com.thesoftwareguild.flooringmaster9001.models.States;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class EncodeDecodeStates {

    public List<States> decode(String FILENAME, String DELIMETER) {
        List<States> statesList = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            //implemented to skip over the header line in text file
            sc.nextLine();
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);
                statesList.add(new States(Integer.parseInt(values[2]), values[0], Double.parseDouble(values[1])));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncodeDecodeStates.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collections.sort(statesList, (States o1, States o2) -> o1.getState().compareToIgnoreCase(o2.getState()));
        return statesList;
    }

    public void encode(boolean testMode, List<States> statesList, String FILENAME, String DELIMETER) {
        if (!testMode) {
            try {
                PrintWriter pw = new PrintWriter(new FileWriter(FILENAME));
                pw.println("State, TaxRate, Id");
                for (States currentTaxes : statesList) {
                    String line
                            = currentTaxes.getState() + DELIMETER
                            + currentTaxes.getTaxRate() + DELIMETER
                            + currentTaxes.getId();

                    pw.println(line);
                    pw.flush();

                }
                pw.close();
            } catch (IOException ex) {
                Logger.getLogger(EncodeDecodeStates.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
