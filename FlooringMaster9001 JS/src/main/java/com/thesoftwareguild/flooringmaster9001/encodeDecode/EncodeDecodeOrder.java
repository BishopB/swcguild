/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.encodeDecode;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.thesoftwareguild.flooringmaster9001.dao.OrderDao;
import com.thesoftwareguild.flooringmaster9001.models.Order;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class EncodeDecodeOrder {

    public void encode(boolean testMode, String fileName, List<Order> orderList) {
        if (!testMode) {
            Map<String, List<Order>> orderMap = orderList.stream().collect(Collectors.groupingBy(Order::getFileName));

            try {
                CSVWriter writer = new CSVWriter(new FileWriter(fileName), ',');
                writer.writeNext(new String[]{"OrderNumber", "CustomerName", "State", "TaxRate", "ProductType", "Area", "CostPerSquareFoot", "LaborCostPerSquareFoot", "MaterialCost", "LaborCost", "Tax", "Total"});

                for (Order currentOrder : orderMap.get(fileName)) {
                    String[] line = new String[]{
                        currentOrder.getId().toString(),//0
                        currentOrder.getOrderNum().toString(),//1
                        currentOrder.getProductId().toString(),//2
                        currentOrder.getStateId().toString(),//3
                        currentOrder.getName(),//4
                        "" + currentOrder.getArea(),//5
                        "" + currentOrder.getMaterialCost(),//6
                        "" + currentOrder.getLaborCost(),//7
                        "" + currentOrder.getTaxCost(),//8
                        "" + currentOrder.getTotalCost(),//9
                        "" + currentOrder.getDate()};//10

                    writer.writeNext(line);
                    writer.flush();
                }
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<Order> decode(File file) {
        List<Order> fileList = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new FileReader(file), ',', '"', '\0');
            String[] values;
            reader.readNext();

            while ((values = reader.readNext()) != null) {
                fileList.add(new Order(Integer.parseInt(values[0]),Integer.parseInt(values[1]), 
                        Integer.parseInt(values[2]), Integer.parseInt(values[3]), values[4], 
                        Double.parseDouble(values[5]), Double.parseDouble(values[6]), 
                        Double.parseDouble(values[7]), Double.parseDouble(values[8]), 
                        Double.parseDouble(values[9]), file.getName(),
                        LocalDate.parse(values[10], DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fileList;
    }
}
