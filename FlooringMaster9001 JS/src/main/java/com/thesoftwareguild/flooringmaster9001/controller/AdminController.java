/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.controller;

import com.thesoftwareguild.flooringmaster9001.dao.ConfigDao;
import com.thesoftwareguild.flooringmaster9001.dao.OrderDao;
import com.thesoftwareguild.flooringmaster9001.dao.ProductDao;
import com.thesoftwareguild.flooringmaster9001.dao.StatesDao;
import com.thesoftwareguild.flooringmaster9001.models.Product;
import com.thesoftwareguild.flooringmaster9001.models.States;
import com.thesoftwareguild.flooringmaster9001.validation.ValidationErrorContainer;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private ConfigDao configDao;
    private OrderDao orderDao;
    private ProductDao productDao;
    private StatesDao statesDao;
    private RCT_Generator rct;

    @Inject
    public AdminController(ConfigDao configDao, StatesDao statesDao, ProductDao productDao, OrderDao orderDao, RCT_Generator rctBean) {
        this.configDao = configDao;
        this.productDao = productDao;
        this.statesDao = statesDao;
        this.orderDao = orderDao;
        this.rct = rctBean;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String adminHome(Model model) {

        model.addAttribute("rct", rct.getTheme());

        return "admin";
    }

    @RequestMapping(value = "/choose", method = RequestMethod.GET)
    public String choosePage(Model model) {

        model.addAttribute("rct", rct.getTheme());

        return "adminChoose";
    }

    @RequestMapping(value = "/states", method = RequestMethod.GET)
    public String statesPage(Model model) {

        model.addAttribute("allTheStates", statesDao.list());
        model.addAttribute("rct", rct.getTheme());

        return "adminStates";
    }

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String productsPage(Model model) {

        model.addAttribute("allTheProducts", productDao.list());
        model.addAttribute("rct", rct.getTheme());

        return "adminProducts";
    }

    //===================================================================
    @RequestMapping(value = "/product", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> createProduct(@Valid @RequestBody Product product) {
        //Test if abbreviation is taken
        if (productDao.list().stream().map(s -> s.getProductType().toLowerCase()).collect(Collectors.toList()).contains(product.getProductType().toLowerCase())) {
            ValidationErrorContainer container = new ValidationErrorContainer();

            container.addError("productType", "That product name is currently being used");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(container);
        }

        product = productDao.createProduct(product);
        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    @RequestMapping(value = "/product/{id}")
    @ResponseBody
    public Product showProduct(@PathVariable("id") Integer productId) {
        //New
        Product product = productDao.getProduct(productId);
        return product;
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteProduct(@PathVariable("id") Integer productId) {
        productDao.removeProduct(productId);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> editProduct(@Valid @RequestBody Product product) {

        //Test if abbreviation is taken by another ID
        if (productDao.list().stream().map(s -> s.getProductType().toLowerCase()).collect(Collectors.toList()).contains(product.getProductType().toLowerCase()) && (!productDao.getProduct(product.getId()).getProductType().equalsIgnoreCase(product.getProductType()))) {
            ValidationErrorContainer container = new ValidationErrorContainer();

            container.addError("productType", "That product name is currently being used");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(container);
        }

        productDao.updateProduct(product);
        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    @RequestMapping(value = "/product/search/{searchValue}")
    @ResponseBody
    public List<Product> searchProduct(@PathVariable("searchValue") String searchValue) {
        searchValue+="";
        return searchProducts(searchValue);
    }

    //==========================================================================================
    @RequestMapping(value = "/state/{id}")
    @ResponseBody
    public States showState(@PathVariable("id") Integer stateId) {
        States state = statesDao.getState(stateId);
        return state;
    }

    @RequestMapping(value = "/state/search/{searchValue}")
    @ResponseBody
    public List<States> searchState(@PathVariable("searchValue") String searchValue) {
        return searchStates(searchValue);
    }

    @RequestMapping(value = "/state", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> createState(@Valid @RequestBody States state) {
        state.setState(state.getState().toUpperCase());

        //Test if abbreviation is taken
        if (statesDao.list().stream().map(s -> s.getState()).collect(Collectors.toList()).contains(state.getState())) {
            ValidationErrorContainer container = new ValidationErrorContainer();

            container.addError("state", "That abbreviation is currently taken");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(container);
        }

        state = statesDao.createState(state);
        return ResponseEntity.status(HttpStatus.OK).body(state);
    }

    @RequestMapping(value = "/state/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteState(@PathVariable("id") Integer stateId) {
        statesDao.removeState(stateId);
    }

    @RequestMapping(value = "/state/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> editState(@Valid @RequestBody States state) {
        state.setState(state.getState().toUpperCase());

        //Test if abbreviation is taken by another ID
        if (statesDao.list().stream().map(s -> s.getState()).collect(Collectors.toList()).contains(state.getState()) && (!statesDao.getState(state.getId()).getState().equals(state.getState()))) {
            ValidationErrorContainer container = new ValidationErrorContainer();

            container.addError("state", "That abbreviation is currently taken");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(container);
        }

        statesDao.updateState(state);
        return ResponseEntity.status(HttpStatus.OK).body(state);
    }

    //=====================================================================
    private List<States> searchStates(String searchValue) {
        return statesDao.list()
                .stream()
                .filter(x -> x.getState().toLowerCase().contains(searchValue.toLowerCase())
                        || (x.getTaxRate() + "").contains(searchValue))
                .collect(Collectors.toList());
    }

    private List<Product> searchProducts(String searchValue) {
        return productDao.list()
                .stream()
                .filter(x -> x.getProductType().toLowerCase().contains(searchValue.toLowerCase())
                        || (x.getLaborCostSqFt() + "").contains(searchValue) || (x.getCostSqFt() + "").contains(searchValue))
                .collect(Collectors.toList());
    }
}
