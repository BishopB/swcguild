package com.thesoftwareguild.flooringmaster9001.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class RCT_Generator {

    private static int lastIndex = 0;

    public String[] getTheme() {
        Random rng = new Random(System.currentTimeMillis());
        String[] rct = new String[2];

        int index = rng.nextInt(5) + (lastIndex > 6 ? 1 : 7);
        lastIndex = index;
        switch (index) {
            case 1:
                rct[0] = "#FEC679";
                rct[1] = "#879BCE";
                break;
            case 2:
                rct[0] = "#F9F18C";
                rct[1] = "#9087C0";
                break;
            case 3:
                rct[0] = "#C1DD89";
                rct[1] = "#AC8BC0";
                break;
            case 4:
                rct[0] = "#ABD595";
                rct[1] = "#DF92BE";
                break;
            case 5:
                rct[0] = "#ABDAC6";
                rct[1] = "#F489A7";
                break;
            case 6:
                rct[0] = "#96D1F3";
                rct[1] = "#F68567";
                break;
            case 7:
                rct[0] = "#879BCE";
                rct[1] = "#FEC679";
                break;
            case 8:
                rct[0] = "#9087C0";
                rct[1] = "#F9F18C";
                break;
            case 9:
                rct[0] = "#AC8BC0";
                rct[1] = "#C1DD89";
                break;
            case 10:
                rct[0] = "#DF92BE";
                rct[1] = "#ABD595";
                break;
            case 11:
                rct[0] = "#F489A7";
                rct[1] = "#ABDAC6";
                break;
            case 12:
                rct[0] = "#F68567";
                rct[1] = "#96D1F3";
                break;
        }

        return rct;
    }

}
