/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.controller;

import com.thesoftwareguild.flooringmaster9001.dao.ConfigDao;
import com.thesoftwareguild.flooringmaster9001.dao.OrderDao;
import com.thesoftwareguild.flooringmaster9001.dao.ProductDao;
import com.thesoftwareguild.flooringmaster9001.dao.StatesDao;
import com.thesoftwareguild.flooringmaster9001.models.Order;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller

public class HomeController {

    private ConfigDao configDao;
    private OrderDao orderDao;
    private ProductDao productDao;
    private StatesDao statesDao;
    private RCT_Generator rct;

    @Inject
    public HomeController(ConfigDao cDao, OrderDao oDao,
            ProductDao pDao, StatesDao tDao, RCT_Generator rctBean) {
        this.configDao = cDao;
        this.orderDao = oDao;
        this.productDao = pDao;
        this.statesDao = tDao;
        this.rct = rctBean;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(@ModelAttribute Order order, Model model) {

        sendLists(model);
        model.addAttribute("order", order);
        model.addAttribute("rct", rct.getTheme());

        return "index";
    }

    private void sendLists(Model model) {
        model.addAttribute("allTheStates", statesDao.list());
        model.addAttribute("allTheProducts", productDao.list());
        model.addAttribute("allTheOrders", orderDao.list());
    }

}
