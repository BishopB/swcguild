/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.validation;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.thesoftwareguild.flooringmaster9001.models.States;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@ControllerAdvice
public class RestValidationHandler {

    //@ExceptionHandler(MethodArgumentNotValidException.class)

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorContainer processValidationErrors(MethodArgumentNotValidException e) {

        BindingResult result = e.getBindingResult();

        List<FieldError> errors = result.getFieldErrors();

        ValidationErrorContainer container = new ValidationErrorContainer();

        for (FieldError fieldError : errors) {
            container.addError(fieldError.getField(), fieldError.getDefaultMessage());
        }

        return container;
    }
//
//    @ExceptionHandler({HttpMessageNotReadableException.class, MethodArgumentNotValidException.class})
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ResponseBody
//    public ValidationErrorContainer processBothErrors( MethodArgumentNotValidException e, HttpMessageNotReadableException x) {
//
//        BindingResult result = e.getBindingResult();
//
//        List<FieldError> errors = result.getFieldErrors();
//
//        ValidationErrorContainer container = new ValidationErrorContainer();
//
//        for (FieldError fieldError : errors) {
//            container.addError(fieldError.getField(), fieldError.getDefaultMessage());
//        }
//
//        String fieldName = "";
//        String causeLog = x.getCause().toString();
//        boolean isGood = false;
//        for (int i = 0; i < causeLog.length(); i++) {
//            if (causeLog.charAt(i) == '"') {
//                isGood = !isGood;
//                continue;
//            }
//            if (isGood) {
//                fieldName += causeLog.charAt(i) + "";
//            }
//        }
//
//        container.addError(fieldName, "invalid " + fieldName + " input");
//
//        return container;
//    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorContainer processFormatErrors(HttpMessageNotReadableException e) {

        ValidationErrorContainer container = new ValidationErrorContainer();

        String fieldName = "";
        String causeLog = e.getCause().toString();
        boolean isGood = false;
        for (int i = 0; i < causeLog.length(); i++) {
            if (causeLog.charAt(i) == '"') {
                isGood = !isGood;
                continue;
            }
            if (isGood) {
                fieldName += causeLog.charAt(i) + "";
            }
        }

        container.addError(fieldName, "invalid " + fieldName + " input");

        return container;
    }

}
